use crate::prelude::*;

pub const STDLIB: &[(&str, Callable)] = &[
    ("print", Callable::Lib(print)),
    ("println", Callable::Lib(println)),
    ("reverse", Callable::Lib(reverse)),
    ("pow", Callable::Lib(pow)),
    ("format", Callable::Lib(format)),
    ("to_num", Callable::Lib(to_num)),
    ("to_string", Callable::Lib(to_string)),
    ("to_list", Callable::Lib(to_list)),
    ("concat", Callable::Lib(concat)),
    ("len", Callable::Lib(len)),
    ("pop", Callable::Lib(pop)),
    ("get_args", Callable::Lib(get_args)),
];

pub fn get_args(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let _ = arguments.expect_arity::<0>(&location)?;

    let mut arguments = std::env::args();
    let mut script_args: Vec<Value> = vec![];
    if let Some(name) = arguments.next() {
        script_args.push(Value::new_string(&name, location.clone()))
    };
    let mut had_separator = false;
    for argument in arguments {
        if argument == "--" && !had_separator {
            had_separator = true;
        } else if had_separator {
            script_args.push(Value::new_string(&argument, location.clone()));
        }
    }

    Ok(Value::new_list(script_args.as_slice(), location))
}

pub fn pop(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let mut list = arguments.expect_arity::<1>(&location)?[0]
        .expect_list()?
        .clone();
    list.pop();

    Ok(Value::new_list(list.as_slice(), location))
}

pub fn len(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let value = &arguments.expect_arity::<1>(&location)?[0];

    Ok(Value::new_number(
        if let Ok(value) = value.expect_string() {
            value.len() as f64
        } else if let Ok(value) = value.expect_list() {
            value.len() as f64
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::String, ValueType::List],
                    actual: value.ty(),
                },
                value.location().clone(),
            ));
        },
        location,
    ))
}

pub fn concat(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let arguments = arguments.expect_min_arity(2, &location)?;

    let mut result = vec![];

    for argument in arguments {
        if let Ok(list) = argument.expect_list() {
            result.append(&mut list.clone());
        } else {
            result.push(argument.clone());
        }
    }

    Ok(Value::new_list(result.as_slice(), location))
}

pub fn to_num(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let argument = &arguments.expect_arity::<1>(&location)?[0];

    if let Ok(value) = argument.expect_bool() {
        let out = if value { 1.0 } else { 0.0 };
        Ok(Value::new_number(out, location))
    } else if let Ok(value) = argument.expect_string() {
        Ok(match value.parse() {
            Ok(value) => Value::new_number(value, location),
            Err(_) => Value::new_nil(location),
        })
    } else {
        return Err(Error::new(
            ErrorKind::UnexpectedValue {
                expected: vec![ValueType::String, ValueType::Bool],
                actual: argument.ty(),
            },
            argument.location().clone(),
        ));
    }
}

pub fn to_list(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let argument = &arguments.expect_arity::<1>(&location)?[0];

    if let Ok((start, end)) = argument.expect_range() {
        let end = match end {
            Some(end) => end,
            None => {
                return Err(Error::new(
                    ErrorKind::Custom(
                        "open-ended ranges are not allowed as arguments to this function"
                            .to_string(),
                    ),
                    argument.location().clone(),
                ))
            }
        };
        Ok(Value::new_list(
            (start as usize..end as usize)
                .map(|v| Value::new_number(v as f64, location.clone()))
                .collect::<Vec<_>>()
                .as_slice(),
            location,
        ))
    } else if let Ok(value) = argument.expect_string() {
        Ok(Value::new_list(
            value
                .chars()
                .map(|v| Value::new_string(&v.to_string(), location.clone()))
                .collect::<Vec<_>>()
                .as_slice(),
            location,
        ))
    } else {
        return Err(Error::new(
            ErrorKind::UnexpectedValue {
                expected: vec![ValueType::String, ValueType::Range],
                actual: argument.ty(),
            },
            argument.location().clone(),
        ));
    }
}

pub fn to_string(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    Ok(Value::new_string(
        &arguments.expect_arity::<1>(&location)?[0].to_string(),
        location,
    ))
}

pub fn print(
    writer: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let mut result = String::new();

    for argument in arguments {
        result.push_str(&argument.to_string());
        result.push(' ');
    }
    result.pop();

    writer.write_all(result.as_bytes()).unwrap();

    Ok(Value::new_nil(location))
}

pub fn println(
    writer: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let mut result = String::new();

    for argument in arguments {
        result.push_str(&argument.to_string());
        result.push(' ');
    }
    result.pop();
    result.push('\n');

    writer.write_all(result.as_bytes()).unwrap();

    Ok(Value::new_nil(location))
}

pub fn format(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let arguments = arguments.expect_min_arity(2, &location)?;

    let template = &arguments[0];
    let mut template = template.expect_string()?.clone();

    for (i, argument) in arguments.iter().enumerate() {
        if i == 0 {
            continue;
        }

        template = template.replacen("{}", &argument.to_string(), 1);
    }

    template = template.replace(r#"\{\}"#, "{}");

    Ok(Value::new_string(&template, location))
}

pub fn reverse(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let value = &arguments.expect_arity::<1>(&location)?[0];
    let value = value.expect_string()?;

    Ok(Value::new_string(value, location))
}

pub fn pow(
    _: &mut dyn std::io::Write,
    arguments: &[Value],
    location: SourceLocation,
) -> Result<Value> {
    let arguments = &arguments.expect_arity::<2>(&location)?;

    let base = &arguments[0];
    let base = base.expect_number()?;

    let power = &arguments[1];
    let power = power.expect_number()?;

    Ok(Value::new_number(base.powf(power), location))
}
