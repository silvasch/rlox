use argh::FromArgs;

/// Rlox interpreter
#[derive(FromArgs)]
struct Args {
    /// print tokens produced by the lexer
    #[argh(switch)]
    print_tokens: bool,

    /// print ast produced by the parser
    #[argh(switch)]
    print_ast: bool,

    /// disable execution of the source code
    #[argh(switch)]
    disable_execution: bool,

    #[argh(subcommand)]
    nested: ArgsSubCommand,
}

#[derive(FromArgs)]
#[argh(subcommand)]
enum ArgsSubCommand {
    Run(RunSubCommand),
    Repl(ReplSubCommand),
    Exec(ExecSubCommand),
}

/// execute code from a file
#[derive(FromArgs)]
#[argh(subcommand, name = "run")]
struct RunSubCommand {
    #[argh(positional)]
    file_path: String,
}

/// execute code in a repl
#[derive(FromArgs)]
#[argh(subcommand, name = "repl")]
struct ReplSubCommand {}

/// provide the code to execute as an argument
#[derive(FromArgs)]
#[argh(subcommand, name = "exec")]
struct ExecSubCommand {
    #[argh(positional)]
    source: String,
}

fn main() {
    let (name, rlox_arguments) = process_env();
    let rlox_arguments = rlox_arguments.iter().map(AsRef::as_ref).collect::<Vec<_>>();

    let args = get_args(&name, rlox_arguments.as_slice());

    match args.nested {
        ArgsSubCommand::Run(RunSubCommand { file_path }) => {
            let source = match std::fs::read_to_string(&file_path) {
                Ok(source) => source,
                Err(_) => todo!(),
            };

            match rlox::Runner::new(
                Some(&file_path),
                args.print_tokens,
                args.print_ast,
                !args.disable_execution,
            )
            .run(&source)
            {
                Ok(_) => {}
                Err(e) => eprintln!("{}", e),
            }
        }
        ArgsSubCommand::Repl(_) => {
            let mut runner = rlox::Runner::new(
                None,
                args.print_tokens,
                args.print_ast,
                !args.disable_execution,
            );

            loop {
                print!("> ");

                std::io::Write::flush(&mut std::io::stdout()).unwrap();
                let mut source = String::new();
                std::io::stdin().read_line(&mut source).unwrap();

                if &source == ".exit\n" {
                    break;
                }

                match runner.run(&source) {
                    Ok(_) => {}
                    Err(e) => eprintln!("{}", e),
                };
            }
        }
        ArgsSubCommand::Exec(ExecSubCommand { source }) => match rlox::Runner::new(
            None,
            args.print_tokens,
            args.print_ast,
            !args.disable_execution,
        )
        .run(&source)
        {
            Ok(_) => {}
            Err(e) => println!("{}", e),
        },
    }
}

// Copyright 2019 The Fuchsia Authors. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:

//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
fn get_args(name: &str, arguments: &[&str]) -> Args {
    Args::from_args(&[name], arguments).unwrap_or_else(|early_exit| {
        std::process::exit(match early_exit.status {
            Ok(()) => {
                println!("{}", early_exit.output);
                0
            }
            Err(()) => {
                eprintln!(
                    "{}\nRun {} --help for more information.",
                    early_exit.output, name
                );
                1
            }
        })
    })
}

fn process_env() -> (String, Vec<String>) {
    let arguments = std::env::args();
    let mut name = "rlox".to_string();
    let mut rlox_arguments: Vec<String> = vec![];

    for (i, argument) in arguments.enumerate() {
        if i == 0 {
            name = argument;
            continue;
        }

        if argument == "--" {
            break;
        } else {
            rlox_arguments.push(argument.clone());
        }
    }

    (name, rlox_arguments)
}
