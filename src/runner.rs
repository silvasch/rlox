use crate::{prelude::*, Interpreter, Lexer, Parser};

pub struct Runner {
    interpreter: Interpreter<std::io::Stdout>,

    file_path: Option<String>,
    print_tokens: bool,
    print_ast: bool,
    execute: bool,
}

impl Runner {
    pub fn new(
        file_path: Option<&str>,
        print_tokens: bool,
        print_ast: bool,
        execute: bool,
    ) -> Self {
        let mut interpreter = Interpreter::default();

        interpreter.add_callables(crate::stdlib::STDLIB);

        Self {
            interpreter,

            file_path: file_path.map(|v| v.to_string()),
            print_tokens,
            print_ast,
            execute,
        }
    }

    pub fn run(&mut self, source: &str) -> Result<()> {
        let tokens = Lexer::new(source, self.file_path.as_deref()).get_tokens()?;
        if self.print_tokens {
            println!("===== TOKENS =====");
            for token in &tokens {
                println!("{}", token);
            }
            println!("==================");
        }

        let statements = Parser::new(
            tokens,
            crate::utils::last_location_from_source(source, self.file_path.as_deref()),
        )
        .get_statements()?;
        if self.print_ast {
            println!("===== AST    =====");
            for statement in &statements {
                println!("{}", statement);
            }
            println!("==================");
        }

        if self.execute {
            self.interpreter
                .interpret_statements(statements)
                .map(|_| ())
        } else {
            Ok(())
        }
    }
}
