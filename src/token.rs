use std::fmt;

use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Token {
    internal: InternalToken,
    location: SourceLocation,
}

impl Token {
    pub fn new(ty: TokenType, location: SourceLocation) -> Self {
        Self {
            internal: match ty {
                TokenType::Number => {
                    panic!("cannot convert TokenType::Number into an InternalToken")
                }
                TokenType::String => {
                    panic!("cannot convert TokenType::String into an InternalToken")
                }
                TokenType::Identifier => {
                    panic!("cannot convert TokenType::Identifier into an InternalToken")
                }

                TokenType::LeftParen => InternalToken::LeftParen,
                TokenType::RightParen => InternalToken::RightParen,
                TokenType::LeftBrace => InternalToken::LeftBrace,
                TokenType::RightBrace => InternalToken::RightBrace,
                TokenType::LeftBracket => InternalToken::LeftBracket,
                TokenType::RightBracket => InternalToken::RightBracket,

                TokenType::Comma => InternalToken::Comma,
                TokenType::Dot => InternalToken::Dot,
                TokenType::Colon => InternalToken::Colon,
                TokenType::Semicolon => InternalToken::Semicolon,

                TokenType::Plus => InternalToken::Plus,
                TokenType::Minus => InternalToken::Minus,
                TokenType::Star => InternalToken::Star,
                TokenType::Slash => InternalToken::Slash,
                TokenType::Percent => InternalToken::Percent,

                TokenType::PlusEqual => InternalToken::PlusEqual,
                TokenType::MinusEqual => InternalToken::MinusEqual,
                TokenType::StarEqual => InternalToken::StarEqual,
                TokenType::SlashEqual => InternalToken::SlashEqual,
                TokenType::PercentEqual => InternalToken::PercentEqual,

                TokenType::EqualEqual => InternalToken::EqualEqual,
                TokenType::BangEqual => InternalToken::BangEqual,
                TokenType::Greater => InternalToken::Greater,
                TokenType::GreaterEqual => InternalToken::GreaterEqual,
                TokenType::Less => InternalToken::Less,
                TokenType::LessEqual => InternalToken::LessEqual,

                TokenType::Bang => InternalToken::Bang,
                TokenType::Equal => InternalToken::Equal,

                TokenType::True => InternalToken::True,
                TokenType::False => InternalToken::False,

                TokenType::If => InternalToken::If,
                TokenType::Else => InternalToken::Else,
                TokenType::And => InternalToken::And,
                TokenType::Or => InternalToken::Or,

                TokenType::Class => InternalToken::Class,
                TokenType::This => InternalToken::This,
                TokenType::Super => InternalToken::Super,

                TokenType::Function => InternalToken::Function,
                TokenType::Return => InternalToken::Return,

                TokenType::For => InternalToken::For,
                TokenType::Foreach => InternalToken::Foreach,
                TokenType::While => InternalToken::While,

                TokenType::Nil => InternalToken::Nil,
                TokenType::Var => InternalToken::Var,

                TokenType::EOF => InternalToken::EOF,
            },
            location,
        }
    }

    pub fn new_number(value: f64, location: SourceLocation) -> Self {
        Self {
            internal: InternalToken::Number(value),
            location,
        }
    }

    pub fn new_string(value: &str, location: SourceLocation) -> Self {
        Self {
            internal: InternalToken::String(value.to_string()),
            location,
        }
    }

    pub fn new_identifier(value: &str, location: SourceLocation) -> Self {
        Self {
            internal: InternalToken::Identifier(value.to_string()),
            location,
        }
    }

    pub fn expect(&self, tys: &[TokenType]) -> Result<()> {
        let self_ty = self.ty();

        for other_ty in tys {
            if &self_ty == other_ty {
                return Ok(());
            }
        }

        Err(Error::new(
            ErrorKind::UnexpectedToken {
                expected: tys.to_vec(),
                actual: self.ty(),
            },
            self.location.clone(),
        ))
    }

    pub fn expect_number(&self) -> Result<f64> {
        if let InternalToken::Number(value) = self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedToken {
                    expected: vec![TokenType::Number],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_string(&self) -> Result<&String> {
        if let InternalToken::String(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedToken {
                    expected: vec![TokenType::Number],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_identifier(&self) -> Result<&String> {
        if let InternalToken::Identifier(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedToken {
                    expected: vec![TokenType::Identifier],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn ty(&self) -> TokenType {
        match self.internal {
            InternalToken::Number(_) => TokenType::Number,
            InternalToken::String(_) => TokenType::String,
            InternalToken::Identifier(_) => TokenType::Identifier,

            InternalToken::LeftParen => TokenType::LeftParen,
            InternalToken::RightParen => TokenType::RightParen,
            InternalToken::LeftBrace => TokenType::LeftBrace,
            InternalToken::RightBrace => TokenType::RightBrace,
            InternalToken::LeftBracket => TokenType::LeftBracket,
            InternalToken::RightBracket => TokenType::RightBracket,

            InternalToken::Comma => TokenType::Comma,
            InternalToken::Dot => TokenType::Dot,
            InternalToken::Colon => TokenType::Colon,
            InternalToken::Semicolon => TokenType::Semicolon,

            InternalToken::Plus => TokenType::Plus,
            InternalToken::Minus => TokenType::Minus,
            InternalToken::Star => TokenType::Star,
            InternalToken::Slash => TokenType::Slash,
            InternalToken::Percent => TokenType::Percent,

            InternalToken::PlusEqual => TokenType::PlusEqual,
            InternalToken::MinusEqual => TokenType::MinusEqual,
            InternalToken::StarEqual => TokenType::StarEqual,
            InternalToken::SlashEqual => TokenType::SlashEqual,
            InternalToken::PercentEqual => TokenType::PercentEqual,

            InternalToken::EqualEqual => TokenType::EqualEqual,
            InternalToken::BangEqual => TokenType::BangEqual,
            InternalToken::Greater => TokenType::Greater,
            InternalToken::GreaterEqual => TokenType::GreaterEqual,
            InternalToken::Less => TokenType::Less,
            InternalToken::LessEqual => TokenType::LessEqual,

            InternalToken::Bang => TokenType::Bang,
            InternalToken::Equal => TokenType::Equal,

            InternalToken::True => TokenType::True,
            InternalToken::False => TokenType::False,

            InternalToken::If => TokenType::If,
            InternalToken::Else => TokenType::Else,

            InternalToken::And => TokenType::And,
            InternalToken::Or => TokenType::Or,

            InternalToken::Class => TokenType::Class,
            InternalToken::This => TokenType::This,
            InternalToken::Super => TokenType::Super,

            InternalToken::Function => TokenType::Function,
            InternalToken::Return => TokenType::Return,

            InternalToken::For => TokenType::For,
            InternalToken::Foreach => TokenType::Foreach,
            InternalToken::While => TokenType::While,

            InternalToken::Nil => TokenType::Nil,
            InternalToken::Var => TokenType::Var,

            InternalToken::EOF => TokenType::EOF,
        }
    }

    pub fn location(&self) -> &SourceLocation {
        &self.location
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self.internal {
                InternalToken::Number(value) => format!("Number({})", value),
                InternalToken::String(value) => format!("String('{}')", value),
                InternalToken::Identifier(value) => format!("Identifier('{}')", value),

                _ => self.ty().to_string(),
            }
        )
    }
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Debug, PartialEq)]
pub enum TokenType {
    Number,
    String,
    Identifier,

    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftBracket,
    RightBracket,

    Comma,
    Dot,
    Colon,
    Semicolon,

    Plus,
    Minus,
    Star,
    Slash,
    Percent,

    PlusEqual,
    MinusEqual,
    StarEqual,
    SlashEqual,
    PercentEqual,

    EqualEqual,
    BangEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    Bang,
    Equal,

    True,
    False,

    If,
    Else,
    And,
    Or,

    Class,
    This,
    Super,

    Function,
    Return,

    For,
    Foreach,
    While,

    Nil,
    Var,

    EOF,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use TokenType::*;

        write!(
            f,
            "{}",
            match self {
                Number => "Number",
                String => "String",
                Identifier => "Identfier",

                LeftParen => "LeftParen",
                RightParen => "RightParen",
                LeftBrace => "LeftBrace",
                RightBrace => "RightBrace",
                LeftBracket => "LeftBracket",
                RightBracket => "RightBracket",

                Comma => "Comma",
                Dot => "Dot",
                Colon => "Colon",
                Semicolon => "Semicolon",

                Plus => "Plus",
                Minus => "Minus",
                Star => "Star",
                Slash => "Slash",
                Percent => "Percent",

                PlusEqual => "PlusEqual",
                MinusEqual => "MinusEqual",
                StarEqual => "StarEqual",
                SlashEqual => "SlashEqual",
                PercentEqual => "PercentEqual",

                EqualEqual => "EqualEqual",
                BangEqual => "BangEqual",
                Greater => "Greater",
                GreaterEqual => "GreaterEqual",
                Less => "Less",
                LessEqual => "LessEqual",

                Bang => "Bang",
                Equal => "Equal",

                True => "True",
                False => "False",

                If => "If",
                Else => "Else",
                And => "And",
                Or => "Or",

                Class => "Class",
                This => "This",
                Super => "Super",

                Function => "Function",
                Return => "Return",

                For => "For",
                Foreach => "Foreach",
                While => "While",

                Nil => "Nil",
                Var => "Var",

                EOF => "EOF",
            }
        )
    }
}

#[derive(Clone, Debug)]
#[allow(clippy::upper_case_acronyms)]
enum InternalToken {
    Number(f64),
    String(String),
    Identifier(String),

    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    LeftBracket,
    RightBracket,

    Comma,
    Dot,
    Colon,
    Semicolon,

    Plus,
    Minus,
    Star,
    Slash,
    Percent,

    PlusEqual,
    MinusEqual,
    StarEqual,
    SlashEqual,
    PercentEqual,

    EqualEqual,
    BangEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    Bang,
    Equal,

    True,
    False,

    If,
    Else,
    And,
    Or,

    Class,
    This,
    Super,

    Function,
    Return,

    For,
    Foreach,
    While,

    Nil,
    Var,

    EOF,
}
