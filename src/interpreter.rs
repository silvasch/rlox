use std::io::{Stdout, Write};

use crate::{prelude::*, Environment};

pub struct Interpreter<W: Write> {
    environment: Environment,
    writer: W,
}

impl<W: Write> Interpreter<W> {
    pub fn new(writer: W) -> Self {
        Self {
            environment: Environment::new(0, None),
            writer,
        }
    }

    pub fn interpret_statements(&mut self, statements: Vec<Statement>) -> Result<Option<Value>> {
        for statement in statements {
            if let Some(value) = self.interpret_statement(statement)? {
                return Ok(Some(value));
            }
        }

        Ok(None)
    }

    fn interpret_statement(&mut self, statement: Statement) -> Result<Option<Value>> {
        if let Ok(expression) = statement.expect_expression() {
            self.evaluate_expression(expression)?;
        } else if let Ok((identifier, expression)) = statement.expect_var() {
            let value = self.evaluate_expression(expression)?;
            self.environment.define(&identifier, value);
        } else if let Ok(statements) = statement.expect_block() {
            self.push_environment();

            let value = self.interpret_statements(statements.clone())?;

            self.pop_environment();
            return Ok(value);
        } else if let Ok((condition, then_statement, else_statement)) = statement.expect_if() {
            let condition = self.evaluate_expression(condition)?.expect_bool()?;

            if condition {
                return self.interpret_statement(*then_statement.clone());
            } else {
                match else_statement {
                    Some(else_statement) => {
                        return self.interpret_statement(*else_statement.clone())
                    }
                    None => {}
                };
            }
        } else if let Ok((condition, statement)) = statement.expect_while() {
            while self.evaluate_expression(condition)?.expect_bool()? {
                if let Some(value) = self.interpret_statement(statement.clone())? {
                    return Ok(Some(value));
                };
            }
        } else if let Ok((decl, condition, increment, statement)) = statement.expect_for() {
            self.push_environment();

            self.interpret_statement(decl.clone())?;
            while self.evaluate_expression(condition)?.expect_bool()? {
                if let Some(value) = self.interpret_statement(statement.clone())? {
                    return Ok(Some(value));
                };
                self.evaluate_expression(increment)?;
            }

            self.pop_environment();
        } else if let Ok((value, capture, statement)) = statement.expect_foreach() {
            let list = self.evaluate_expression(value)?.expect_list()?.clone();

            self.push_environment();

            self.environment
                .define(capture, Value::new_nil(value.location().clone()));

            for current_value in list {
                self.environment
                    .assign(capture, current_value, value.location())?;
                if let Some(value) = self.interpret_statement(statement.clone())? {
                    return Ok(Some(value));
                }
            }

            self.pop_environment();
        } else if let Ok(expression) = statement.expect_return() {
            return Ok(Some(self.evaluate_expression(expression)?));
        } else {
            unreachable!()
        }

        Ok(None)
    }

    fn evaluate_expression(&mut self, expression: &Expression) -> Result<Value> {
        if let Ok(identifier) = expression.expect_identifier() {
            return self
                .environment
                .get(identifier, expression.location())
                .cloned();
        } else if let Ok(value) = expression.expect_number() {
            return Ok((value, expression.location().clone()).into());
        } else if let Ok(value) = expression.expect_string() {
            return Ok((value.clone(), expression.location().clone()).into());
        } else if let Ok(value) = expression.expect_bool() {
            return Ok((value, expression.location().clone()).into());
        } else if let Ok((to_index, index)) = expression.expect_index() {
            let to_index = self.evaluate_expression(to_index)?.expect_list()?.clone();
            let length = to_index.len() as isize;

            let index = self.evaluate_expression(index)?;
            if let Ok(index) = index.expect_number() {
                let index = index.round() as isize;
                let index = if index < 0 { length + index } else { index };

                if index >= length || index < 0 {
                    return Err(Error::new(
                        ErrorKind::InvalidIndex {
                            index,
                            length: length as usize,
                        },
                        expression.location().clone(),
                    ));
                } else {
                    return Ok(to_index.get(index as usize).unwrap().clone());
                }
            } else if let Ok((start, end)) = index.expect_range() {
                let end = end.unwrap_or(length - 1);

                if start >= end {
                    return Err(Error::new(
                        ErrorKind::RangeStartGreaterThanEnd,
                        expression.location().clone(),
                    ));
                } else if start >= length || start < 0 {
                    return Err(Error::new(
                        ErrorKind::InvalidIndex {
                            index: start,
                            length: length as usize,
                        },
                        expression.location().clone(),
                    ));
                } else if end >= length || end < 0 {
                    return Err(Error::new(
                        ErrorKind::InvalidIndex {
                            index: end,
                            length: length as usize,
                        },
                        expression.location().clone(),
                    ));
                } else {
                    return Ok(Value::new_list(
                        to_index.get(start as usize..end as usize + 1).unwrap(),
                        expression.location().clone(),
                    ));
                }
            };

            return Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::Number, ValueType::Range],
                    actual: index.ty().clone(),
                },
                index.location().clone(),
            ));
        } else if let Ok(value) = expression.expect_list() {
            let mut values = vec![];
            for value in value {
                values.push(self.evaluate_expression(value)?);
            }
            return Ok(Value::new_list(
                values.as_slice(),
                expression.location().clone(),
            ));
        } else if let Ok((start, end)) = expression.expect_range() {
            match (start, end) {
                (Some(start), Some(end)) => {
                    return Ok(Value::new_range(
                        self.evaluate_expression(start)?.expect_number()?.round() as isize,
                        Some(self.evaluate_expression(end)?.expect_number()?.round() as isize),
                        expression.location().clone(),
                    ))
                }
                (Some(start), None) => {
                    return Ok(Value::new_range(
                        self.evaluate_expression(start)?.expect_number()?.round() as isize,
                        None,
                        expression.location().clone(),
                    ))
                }
                (None, Some(end)) => {
                    return Ok(Value::new_range(
                        0,
                        Some(self.evaluate_expression(end)?.expect_number()?.round() as isize),
                        expression.location().clone(),
                    ))
                }
                (None, None) => {
                    return Err(Error::new(
                        ErrorKind::EmptyRange,
                        expression.location().clone(),
                    ))
                }
            }
        } else if expression.expect_nil().is_ok() {
            return Ok(Value::new_nil(expression.location().clone()));
        } else if let Ok(expression) = expression.expect_group() {
            return self.evaluate_expression(expression);
        } else if let Ok((identifier, expression)) = expression.expect_assign() {
            let value = self.evaluate_expression(expression)?;
            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok((identifier, expression)) = expression.expect_addition_assign() {
            let value = self.evaluate_expression(expression)?;
            let previous_value = self.environment.get(identifier, expression.location())?;

            let value = if let Ok(previous_value) = previous_value.expect_number() {
                let value = value.expect_number()?;
                Value::new_number(value + previous_value, expression.location().clone())
            } else if let Ok(previous_value) = previous_value.expect_string() {
                let value = value.expect_string()?;
                Value::new_string(
                    &format!("{}{}", previous_value, value),
                    expression.location().clone(),
                )
            } else {
                return Err(Error::new(
                    ErrorKind::UnexpectedValue {
                        expected: vec![ValueType::Number, ValueType::String],
                        actual: value.ty(),
                    },
                    value.location().clone(),
                ));
            };

            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok((identifier, expression)) = expression.expect_subtraction_assign() {
            let value = self.evaluate_expression(expression)?.expect_number()?;
            let previous_value = self
                .environment
                .get(identifier, expression.location())?
                .expect_number()?;
            let value = Value::new_number(previous_value - value, expression.location().clone());
            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok((identifier, expression)) = expression.expect_multiplication_assign() {
            let value = self.evaluate_expression(expression)?.expect_number()?;
            let previous_value = self
                .environment
                .get(identifier, expression.location())?
                .expect_number()?;
            let value = Value::new_number(value * previous_value, expression.location().clone());
            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok((identifier, expression)) = expression.expect_division_assign() {
            let value = self.evaluate_expression(expression)?.expect_number()?;
            let previous_value = self
                .environment
                .get(identifier, expression.location())?
                .expect_number()?;
            let value = Value::new_number(previous_value / value, expression.location().clone());
            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok((identifier, expression)) = expression.expect_modulo_assign() {
            let value = self.evaluate_expression(expression)?.expect_number()?;
            let previous_value = self
                .environment
                .get(identifier, expression.location())?
                .expect_number()?;
            let value = Value::new_number(previous_value % value, expression.location().clone());
            self.environment
                .assign(identifier, value.clone(), expression.location())?;
            return Ok(value);
        } else if let Ok(expression) = expression.expect_not() {
            let value = self.evaluate_expression(expression)?.expect_bool()?;
            return Ok((!value, expression.location().clone()).into());
        } else if let Ok(expression) = expression.expect_negate() {
            let value = self.evaluate_expression(expression)?.expect_number()?;
            return Ok((-value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_and() {
            let left_value = self.evaluate_expression(left)?.expect_bool()?;
            let right_value = self.evaluate_expression(right)?.expect_bool()?;

            return Ok((left_value && right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_or() {
            let left_value = self.evaluate_expression(left)?.expect_bool()?;
            let right_value = self.evaluate_expression(right)?.expect_bool()?;

            return Ok((left_value || right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_equal() {
            let left_value = self.evaluate_expression(left)?;
            let right_value = self.evaluate_expression(right)?;

            return Ok((left_value == right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_not_equal() {
            let left_value = self.evaluate_expression(left)?;
            let right_value = self.evaluate_expression(right)?;

            return Ok((left_value != right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_greater() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;

            return Ok((left_value > right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_greater_equal() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;

            return Ok((left_value >= right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_less() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;

            return Ok((left_value < right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_less_equal() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;

            return Ok((left_value <= right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_addition() {
            let left_value = self.evaluate_expression(left)?;
            let left_value_type = left_value.ty();
            let right_value = self.evaluate_expression(right)?;

            if let Ok(left_value) = left_value.expect_number() {
                let right_value = right_value.expect_number()?;
                return Ok((left_value + right_value, expression.location().clone()).into());
            } else if let Ok(left_value) = left_value.expect_string() {
                let right_value = right_value.expect_string()?;
                return Ok((
                    format!("{}{}", left_value, right_value),
                    expression.location().clone(),
                )
                    .into());
            } else {
                return Err(Error::new(
                    ErrorKind::UnexpectedValue {
                        expected: vec![ValueType::Number, ValueType::String],
                        actual: left_value_type,
                    },
                    left.location().clone(),
                ));
            }
        } else if let Ok((left, right)) = expression.expect_subtraction() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;
            return Ok((left_value - right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_multiplication() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;
            return Ok((left_value * right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_division() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;
            return Ok((left_value / right_value, expression.location().clone()).into());
        } else if let Ok((left, right)) = expression.expect_modulo() {
            let left_value = self.evaluate_expression(left)?.expect_number()?;
            let right_value = self.evaluate_expression(right)?.expect_number()?;
            return Ok((left_value % right_value, expression.location().clone()).into());
        } else if let Ok((argument_names, body)) = expression.expect_function() {
            return Ok(Value::new_callable(
                Callable::User {
                    argument_names: argument_names.clone(),
                    body: body.clone(),
                },
                expression.location().clone(),
            ));
        } else if let Ok((callee, arguments)) = expression.expect_call() {
            let identifier = callee.expect_identifier()?;
            let callable = self
                .environment
                .get(identifier, callee.location())?
                .expect_callable(callee.location())?
                .clone();

            let mut new_arguments = vec![];

            for argument in arguments {
                new_arguments.push(self.evaluate_expression(argument)?);
            }

            match callable {
                Callable::Lib(callable) => {
                    return callable(
                        &mut self.writer,
                        new_arguments.as_slice(),
                        expression.location().clone(),
                    );
                }

                Callable::User {
                    argument_names,
                    body,
                } => {
                    let new_environment = Environment::new(
                        self.environment.id() + 1,
                        Some(Box::new(self.environment.clone())),
                    );
                    self.environment = new_environment;

                    for (name, value) in argument_names.iter().zip(new_arguments) {
                        self.environment.define(name, value);
                    }

                    let value = if let Ok(expression) = body.expect_return() {
                        self.evaluate_expression(expression)?
                    } else if let Ok(block) = body.expect_block() {
                        let mut value = Value::new_nil(body.location().clone());

                        for statement in block {
                            if let Some(v) = self.interpret_statement(statement.clone())? {
                                value = v;
                                break;
                            }
                        }

                        value
                    } else {
                        let location = body.location().clone();
                        self.interpret_statement(body)?;
                        Value::new_nil(location)
                    };

                    self.environment = *(self.environment.clone().parent().unwrap());

                    return Ok(value);
                }
            }
        }

        panic!("this function should handle all cases!");
    }

    fn push_environment(&mut self) {
        self.environment = Environment::new(
            self.environment.id() + 1,
            Some(Box::new(self.environment.clone())),
        );
    }

    fn pop_environment(&mut self) {
        self.environment = *(self.environment.clone().parent().unwrap());
    }

    pub fn add_callables(&mut self, callables: &[(&str, Callable)]) {
        for (identifier, callable) in callables {
            self.add_callable(identifier, callable.clone());
        }
    }

    pub fn add_callable(&mut self, identifier: &str, callable: Callable) {
        self.environment
            .define(identifier, Value::new_callable(callable, (0, 0).into()));
    }
}

impl Default for Interpreter<Stdout> {
    fn default() -> Self {
        let mut interpreter = Self::new(std::io::stdout());

        interpreter.add_callables(crate::stdlib::STDLIB);

        interpreter
    }
}

#[cfg(test)]
mod tests {
    use crate::{lexer::Lexer, parser::Parser};

    use super::*;

    #[test]
    fn if_test() {
        let source = r#"if (true) {
    println("is true");
} else {
    println("is false");          
}"#;
        let last_location = crate::utils::last_location_from_source(source, None);
        let expected = "is true\n";

        test_interpreter(source, expected, last_location);
    }

    #[test]
    fn while_loop_test() {
        let source = r#"var x = 0;
while (x < 10) { 
    x = x + 1;
    println(x);
}"#;
        let last_location = crate::utils::last_location_from_source(source, None);
        let expected = "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n";

        test_interpreter(source, expected, last_location);
    }

    #[test]
    fn for_loop_test() {
        let source = r#"for (var i = 0; i < 10; i = i + 1) {
            println(i);
        }"#;
        let last_location = crate::utils::last_location_from_source(source, None);
        let expected = "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n";

        test_interpreter(source, expected, last_location);
    }

    #[test]
    fn environments_test() {
        let source = r#"var a = "global a";
var b = "global b";
var c = "global c";
{
  var a = "outer a";
  var b = "outer b";
  {
    var a = "inner a";
    println(a);
    println(b);
    println(c);
  }
  println(a);
  println(b);
  println(c);
}
println(a);
println(b);
println(c);"#;
        let last_location = crate::utils::last_location_from_source(source, None);
        let expected = r#"inner a
outer b
global c
outer a
outer b
global c
global a
global b
global c
"#;
        test_interpreter(source, expected, last_location);
    }

    fn test_interpreter(source: &str, expected: &str, last_location: SourceLocation) {
        let mut actual: Vec<u8> = vec![];

        let mut interpreter = Interpreter::new(&mut actual);
        interpreter.add_callables(crate::stdlib::STDLIB);

        interpreter
            .interpret_statements(
                Parser::new(
                    Lexer::new(source, None).get_tokens().unwrap(),
                    last_location,
                )
                .get_statements()
                .unwrap(),
            )
            .unwrap();

        assert_eq!(expected, String::from_utf8_lossy(&actual));
    }
}
