use std::fmt;

use crate::prelude::*;

#[derive(Clone, Debug, PartialEq)]
pub struct Statement {
    internal: InternalStatement,
    location: SourceLocation,
}

impl Statement {
    pub fn new_expression(expression: Expression, location: SourceLocation) -> Self {
        Self {
            internal: InternalStatement::Expression(expression),
            location,
        }
    }

    pub fn new_var(identifier: &str, expression: Expression, location: SourceLocation) -> Self {
        Self {
            internal: InternalStatement::Var {
                identifier: identifier.to_string(),
                expression,
            },
            location,
        }
    }

    pub fn new_return(expression: Expression, location: SourceLocation) -> Self {
        Self {
            internal: InternalStatement::Return(expression),
            location,
        }
    }

    pub fn new_block(statements: Vec<Statement>, location: SourceLocation) -> Self {
        Self {
            internal: InternalStatement::Block(statements),
            location,
        }
    }

    pub fn new_if(
        condition: Expression,
        then_statement: Box<Statement>,
        else_statement: Option<Box<Statement>>,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalStatement::If {
                condition,
                then_statement,
                else_statement,
            },
            location,
        }
    }

    pub fn new_while(
        condition: Expression,
        statement: Box<Statement>,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalStatement::While {
                condition,
                statement,
            },
            location,
        }
    }

    pub fn new_for(
        decl: Box<Statement>,
        condition: Expression,
        increment: Expression,
        statement: Box<Statement>,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalStatement::For {
                decl,
                condition,
                increment,
                statement,
            },
            location,
        }
    }

    pub fn new_foreach(
        value: Expression,
        capture: &str,
        statement: Box<Statement>,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalStatement::Foreach {
                value,
                capture: capture.to_string(),
                statement,
            },
            location,
        }
    }

    pub fn expect_expression(&self) -> Result<&Expression> {
        if let InternalStatement::Expression(expression) = &self.internal {
            Ok(expression)
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Expression],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_var(&self) -> Result<(String, &Expression)> {
        if let InternalStatement::Var {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier.to_string(), expression))
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Var],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_return(&self) -> Result<&Expression> {
        if let InternalStatement::Return(expression) = &self.internal {
            Ok(expression)
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Expression],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_block(&self) -> Result<&Vec<Statement>> {
        if let InternalStatement::Block(statements) = &self.internal {
            Ok(statements)
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Block],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    #[allow(clippy::type_complexity)]
    pub fn expect_if(&self) -> Result<(&Expression, &Box<Statement>, &Option<Box<Statement>>)> {
        if let InternalStatement::If {
            condition,
            then_statement,
            else_statement,
        } = &self.internal
        {
            Ok((condition, then_statement, else_statement))
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Block],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_while(&self) -> Result<(&Expression, &Statement)> {
        if let InternalStatement::While {
            condition,
            statement,
        } = &self.internal
        {
            Ok((condition, statement))
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::While],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_for(&self) -> Result<(&Statement, &Expression, &Expression, &Statement)> {
        if let InternalStatement::For {
            decl,
            condition,
            increment,
            statement,
        } = &self.internal
        {
            Ok((decl, condition, increment, statement))
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::For],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn expect_foreach(&self) -> Result<(&Expression, &String, &Statement)> {
        if let InternalStatement::Foreach {
            value,
            capture,
            statement,
        } = &self.internal
        {
            Ok((value, capture, statement))
        } else {
            return Err(Error::new(
                ErrorKind::UnexpectedStatement {
                    expected: vec![StatementType::Foreach],
                    actual: self.ty(),
                },
                self.location().clone(),
            ));
        }
    }

    pub fn ty(&self) -> StatementType {
        match self.internal {
            InternalStatement::Expression(_) => StatementType::Expression,
            InternalStatement::Var {
                identifier: _,
                expression: _,
            } => StatementType::Var,
            InternalStatement::Return(_) => StatementType::Return,
            InternalStatement::Block(_) => StatementType::Block,
            InternalStatement::If { .. } => StatementType::If,
            InternalStatement::While { .. } => StatementType::While,
            InternalStatement::For { .. } => StatementType::For,
            InternalStatement::Foreach { .. } => StatementType::Foreach,
        }
    }

    pub fn location(&self) -> &SourceLocation {
        &self.location
    }
}

impl fmt::Display for Statement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self.internal {
                InternalStatement::Expression(expression) => format!("{};", expression),
                InternalStatement::Var {
                    identifier,
                    expression,
                } => format!("var {} = {}", identifier, expression),
                InternalStatement::Return(expression) => format!("return {};", expression),
                InternalStatement::Block(statements) => {
                    let mut result = "{\n".to_string();

                    for statement in statements {
                        result.push_str(&format!("  {}", statement));
                    }

                    result.push('}');

                    result
                }
                InternalStatement::If {
                    condition,
                    then_statement,
                    else_statement,
                } => {
                    format!(
                        "if ({}) {}{}",
                        condition,
                        then_statement,
                        match else_statement {
                            Some(else_statement) => format!(" else {}", else_statement),
                            None => ";".to_string(),
                        }
                    )
                }
                InternalStatement::While {
                    condition,
                    statement,
                } => format!("while ({}) {}", condition, statement),
                InternalStatement::For {
                    decl,
                    condition,
                    increment,
                    statement,
                } => format!("for ({} {}; {}) {}", decl, condition, increment, statement),
                InternalStatement::Foreach {
                    value,
                    capture,
                    statement,
                } => format!("foreach ({}; {}) {}", value, capture, statement),
            }
        )
    }
}

#[derive(Clone, Debug)]
pub enum StatementType {
    Expression,
    Print,
    Var,
    Return,
    Block,
    If,
    While,
    For,
    Foreach,
}

impl fmt::Display for StatementType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                StatementType::Expression => "ExpressionStatement",
                StatementType::Print => "PrintStatement",
                StatementType::Return => "ReturnStatement",
                StatementType::Var => "VarStatement",
                StatementType::Block => "BlockStatement",
                StatementType::If => "IfStatement",
                StatementType::While => "WhileStatement",
                StatementType::For => "ForStatement",
                StatementType::Foreach => "ForeachStatement",
            }
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
enum InternalStatement {
    Expression(Expression),
    Var {
        identifier: String,
        expression: Expression,
    },
    Return(Expression),
    Block(Vec<Statement>),
    If {
        condition: Expression,
        then_statement: Box<Statement>,
        else_statement: Option<Box<Statement>>,
    },
    While {
        condition: Expression,
        statement: Box<Statement>,
    },
    For {
        decl: Box<Statement>,
        condition: Expression,
        increment: Expression,
        statement: Box<Statement>,
    },
    Foreach {
        value: Expression,
        capture: String,
        statement: Box<Statement>,
    },
}
