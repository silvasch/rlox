use std::io::Write;

use crate::prelude::*;

#[derive(Clone, Debug, PartialEq)]
pub enum Callable {
    Lib(fn(&mut dyn Write, &[Value], SourceLocation) -> Result<Value>),
    User {
        argument_names: Vec<String>,
        body: Statement,
    },
}
