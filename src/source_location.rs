use std::fmt;

#[derive(Clone, Debug)]
pub struct SourceLocation {
    line: usize,
    column: usize,

    file_path: Option<String>,
    snippet: Option<String>,
}

impl SourceLocation {
    pub fn builder(line: usize, column: usize) -> SourceLocationBuilder {
        SourceLocationBuilder::new(line, column)
    }

    pub fn line(&self) -> usize {
        self.line
    }

    pub fn column(&self) -> usize {
        self.column
    }

    pub fn file_path(&self) -> &Option<String> {
        &self.file_path
    }

    pub fn snippet(&self) -> &Option<String> {
        &self.snippet
    }
}

impl PartialEq for SourceLocation {
    fn eq(&self, other: &Self) -> bool {
        self.line == other.line && self.column == other.column
    }
}

impl From<(usize, usize)> for SourceLocation {
    fn from(value: (usize, usize)) -> Self {
        SourceLocation::builder(value.0, value.1).build()
    }
}

impl fmt::Display for SourceLocation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result = format!("line {}, column {}", self.line, self.column);

        match &self.file_path {
            Some(file_path) => result.push_str(&format!(" in file '{}'", file_path)),
            None => {}
        };

        write!(f, "{}", result)
    }
}

pub struct SourceLocationBuilder {
    line: usize,
    column: usize,

    file_path: Option<String>,
    snippet: Option<String>,
}

impl SourceLocationBuilder {
    pub fn new(line: usize, column: usize) -> Self {
        Self {
            line,
            column,
            file_path: None,
            snippet: None,
        }
    }

    pub fn file_path(mut self, file_path: &str) -> Self {
        self.file_path = Some(file_path.to_string());
        self
    }

    pub fn snippet(mut self, snippet: &str) -> Self {
        self.snippet = Some(snippet.to_string());
        self
    }

    pub fn build(self) -> SourceLocation {
        SourceLocation {
            line: self.line,
            column: self.column,
            file_path: self.file_path,
            snippet: self.snippet,
        }
    }
}
