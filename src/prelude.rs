pub(crate) use crate::{
    Callable, Error, ErrorKind, ExpectArity, Expression, ExpressionType, SourceLocation, Statement,
    StatementType, Token, TokenType, Value, ValueType,
};

pub type Result<T> = std::result::Result<T, Error>;

pub trait UnwrapFmt<T> {
    fn unwrap_fmt(self) -> T;
}

impl<T> UnwrapFmt<T> for Result<T> {
    fn unwrap_fmt(self) -> T {
        self.unwrap_or_else(|e| panic!("{}", e))
    }
}

pub trait ExpectExpression {
    fn expect_expression(&self, location: &SourceLocation) -> Result<Expression>;
}

impl ExpectExpression for Option<Expression> {
    fn expect_expression(&self, location: &SourceLocation) -> Result<Expression> {
        match self {
            Some(expression) => Ok(expression.clone()),
            None => Err(Error::new(ErrorKind::ExpectedExpression, location.clone())),
        }
    }
}

pub trait ExpectToken {
    fn expect_token(&self, expected: &[TokenType], location: &SourceLocation) -> Result<&Token>;
}

impl ExpectToken for Option<&Token> {
    fn expect_token(&self, expected: &[TokenType], location: &SourceLocation) -> Result<&Token> {
        match self {
            Some(token) => Ok(*token),
            None => Err(Error::new(
                ErrorKind::UnexpectedToken {
                    expected: expected.to_vec(),
                    actual: TokenType::EOF,
                },
                location.clone(),
            )),
        }
    }
}
