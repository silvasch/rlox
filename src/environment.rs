use std::collections::HashMap;

use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Environment {
    id: usize,
    parent: Option<Box<Environment>>,
    values: HashMap<String, Value>,
}

impl Environment {
    pub fn new(id: usize, parent: Option<Box<Environment>>) -> Self {
        Self {
            id,
            parent,
            values: HashMap::new(),
        }
    }

    pub fn define(&mut self, identifier: &str, value: Value) {
        self.values.insert(identifier.to_string(), value);
    }

    pub fn assign(
        &mut self,
        identifier: &str,
        value: Value,
        location: &SourceLocation,
    ) -> Result<()> {
        if self.values.contains_key(identifier) {
            self.values.insert(identifier.to_string(), value);
            Ok(())
        } else {
            match &mut self.parent {
                Some(parent) => parent.assign(identifier, value, location),
                None => Err(Error::new(
                    ErrorKind::UndefinedVariable(identifier.to_string()),
                    location.clone(),
                )),
            }
        }
    }

    pub fn get(&self, identifier: &str, location: &SourceLocation) -> Result<&Value> {
        match self.values.get(identifier) {
            Some(value) => Ok(value),
            None => match &self.parent {
                Some(parent) => parent.get(identifier, location),
                None => Err(Error::new(
                    ErrorKind::UndefinedVariable(identifier.to_string()),
                    location.clone(),
                )),
            },
        }
    }

    pub fn parent(self) -> Option<Box<Self>> {
        self.parent
    }

    pub fn id(&self) -> usize {
        self.id
    }
}
