use std::fmt;

use crate::prelude::*;

#[derive(Clone, Debug, PartialEq)]
pub struct Expression {
    internal: InternalExpression,
    location: SourceLocation,
}

impl Expression {
    pub fn new_identifier(value: &str, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Identifier(value.to_string()),
            location,
        }
    }

    pub fn new_call(
        callee: Expression,
        arguments: &[Expression],
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::Call {
                callee: Box::new(callee),
                arguments: arguments.to_vec(),
            },
            location,
        }
    }

    pub fn new_function(
        argument_names: &[&str],
        body: Statement,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::Function {
                argument_names: argument_names.iter().map(|v| v.to_string()).collect(),
                body: Box::new(body),
            },
            location,
        }
    }

    pub fn new_unary(ty: ExpressionType, expression: Expression, location: SourceLocation) -> Self {
        let internal = match ty {
            ExpressionType::Not => InternalExpression::Not(Box::new(expression)),
            ExpressionType::Negate => InternalExpression::Negate(Box::new(expression)),
            _ => panic!("the type '{}' is not a unary expression", ty),
        };

        Expression { internal, location }
    }

    pub fn new_binary(
        ty: ExpressionType,
        left: Expression,
        right: Expression,
        location: SourceLocation,
    ) -> Self {
        let left = Box::new(left);
        let right = Box::new(right);

        let internal = match ty {
            ExpressionType::And => InternalExpression::And { left, right },
            ExpressionType::Or => InternalExpression::Or { left, right },
            ExpressionType::Equal => InternalExpression::Equal { left, right },
            ExpressionType::NotEqual => InternalExpression::NotEqual { left, right },
            ExpressionType::Greater => InternalExpression::Greater { left, right },
            ExpressionType::GreaterEqual => InternalExpression::GreaterEqual { left, right },
            ExpressionType::Less => InternalExpression::Less { left, right },
            ExpressionType::LessEqual => InternalExpression::LessEqual { left, right },
            ExpressionType::Addition => InternalExpression::Addition { left, right },
            ExpressionType::Subtraction => InternalExpression::Subtraction { left, right },
            ExpressionType::Multiplication => InternalExpression::Multiplication { left, right },
            ExpressionType::Division => InternalExpression::Division { left, right },
            ExpressionType::Modulo => InternalExpression::Modulo { left, right },
            _ => panic!("the type '{}' is not a binary expression", ty),
        };

        Expression { internal, location }
    }

    pub fn new_number(value: f64, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Number(value),
            location,
        }
    }

    pub fn new_string(value: &str, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::String(value.to_string()),
            location,
        }
    }

    pub fn new_bool(value: bool, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Bool(value),
            location,
        }
    }

    pub fn new_nil(location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Nil,
            location,
        }
    }

    pub fn new_index(to_index: Expression, index: Expression, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Index {
                to_index: Box::new(to_index),
                index: Box::new(index),
            },
            location,
        }
    }

    pub fn new_range(
        start: Option<Box<Expression>>,
        end: Option<Box<Expression>>,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::Range { start, end },
            location,
        }
    }

    pub fn new_list(value: &[Expression], location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::List(value.to_vec()),
            location,
        }
    }

    pub fn new_group(expression: Expression, location: SourceLocation) -> Self {
        Self {
            internal: InternalExpression::Group(Box::new(expression)),
            location,
        }
    }

    pub fn new_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::Assign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn new_addition_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::AdditionAssign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn new_subtraction_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::SubtractionAssign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn new_multiplication_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::MultiplicationAssign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn new_division_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::DivisionAssign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn new_modulo_assign(
        identifier: String,
        expression: Expression,
        location: SourceLocation,
    ) -> Self {
        Self {
            internal: InternalExpression::ModuloAssign {
                identifier,
                expression: Box::new(expression),
            },
            location,
        }
    }

    pub fn expect_identifier(&self) -> Result<&String> {
        if let InternalExpression::Identifier(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Identifier],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_call(&self) -> Result<(&Expression, &Vec<Expression>)> {
        if let InternalExpression::Call { callee, arguments } = &self.internal {
            Ok((callee, arguments))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Call],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_function(&self) -> Result<(&Vec<String>, &Statement)> {
        if let InternalExpression::Function {
            argument_names,
            body,
        } = &self.internal
        {
            Ok((argument_names, body))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Call],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_number(&self) -> Result<f64> {
        if let InternalExpression::Number(value) = &self.internal {
            Ok(*value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Number],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_string(&self) -> Result<&String> {
        if let InternalExpression::String(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::String],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_bool(&self) -> Result<bool> {
        if let InternalExpression::Bool(value) = &self.internal {
            Ok(*value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_nil(&self) -> Result<()> {
        if let InternalExpression::Nil = &self.internal {
            Ok(())
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_index(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Index { to_index, index } = &self.internal {
            Ok((to_index, index))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    #[allow(clippy::type_complexity)]
    pub fn expect_range(&self) -> Result<(&Option<Box<Expression>>, &Option<Box<Expression>>)> {
        if let InternalExpression::Range { start, end } = &self.internal {
            Ok((start, end))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_list(&self) -> Result<&Vec<Expression>> {
        if let InternalExpression::List(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_group(&self) -> Result<&Expression> {
        if let InternalExpression::Group(expression) = &self.internal {
            Ok(expression)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::Assign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Assign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_addition_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::AdditionAssign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::AdditionAssign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_subtraction_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::SubtractionAssign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::SubtractionAssign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_multiplication_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::MultiplicationAssign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::MultiplicationAssign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_division_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::DivisionAssign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::DivisionAssign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_modulo_assign(&self) -> Result<(&String, &Expression)> {
        if let InternalExpression::ModuloAssign {
            identifier,
            expression,
        } = &self.internal
        {
            Ok((identifier, expression))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::ModuloAssign],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_not(&self) -> Result<&Expression> {
        if let InternalExpression::Not(expression) = &self.internal {
            Ok(expression)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Nil],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_negate(&self) -> Result<&Expression> {
        if let InternalExpression::Negate(expression) = &self.internal {
            Ok(expression)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Negate],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_and(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::And { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::And],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_or(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Or { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Or],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_equal(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Equal { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Equal],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_not_equal(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::NotEqual { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::NotEqual],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_greater(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Greater { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Greater],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_greater_equal(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::GreaterEqual { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::GreaterEqual],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_less(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Less { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Less],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }
    pub fn expect_less_equal(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::LessEqual { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::LessEqual],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_addition(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Addition { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Addition],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_subtraction(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Subtraction { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Subtraction],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_multiplication(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Multiplication { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Multiplication],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_division(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Division { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Division],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn expect_modulo(&self) -> Result<(&Expression, &Expression)> {
        if let InternalExpression::Modulo { left, right } = &self.internal {
            Ok((left, right))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedExpression {
                    expected: vec![ExpressionType::Modulo],
                    actual: self.ty(),
                },
                self.location().clone(),
            ))
        }
    }

    pub fn ty(&self) -> ExpressionType {
        match self.internal {
            InternalExpression::Identifier(_) => ExpressionType::Identifier,

            InternalExpression::Number(_) => ExpressionType::Number,
            InternalExpression::String(_) => ExpressionType::String,
            InternalExpression::Bool(_) => ExpressionType::Bool,
            InternalExpression::Nil => ExpressionType::Nil,

            InternalExpression::Index { .. } => ExpressionType::Index,
            InternalExpression::List(_) => ExpressionType::List,
            InternalExpression::Range { .. } => ExpressionType::Range,

            InternalExpression::Assign { .. } => ExpressionType::Assign,
            InternalExpression::AdditionAssign { .. } => ExpressionType::AdditionAssign,
            InternalExpression::SubtractionAssign { .. } => ExpressionType::SubtractionAssign,
            InternalExpression::MultiplicationAssign { .. } => ExpressionType::MultiplicationAssign,
            InternalExpression::DivisionAssign { .. } => ExpressionType::DivisionAssign,
            InternalExpression::ModuloAssign { .. } => ExpressionType::ModuloAssign,

            InternalExpression::Group(_) => ExpressionType::Group,

            InternalExpression::Call { .. } => ExpressionType::Call,
            InternalExpression::Function { .. } => ExpressionType::Function,

            InternalExpression::And { .. } => ExpressionType::And,
            InternalExpression::Or { .. } => ExpressionType::Or,

            InternalExpression::Not(_) => ExpressionType::Not,
            InternalExpression::Negate(_) => ExpressionType::Negate,

            InternalExpression::Equal { .. } => ExpressionType::Equal,
            InternalExpression::NotEqual { .. } => ExpressionType::NotEqual,
            InternalExpression::Less { .. } => ExpressionType::Less,
            InternalExpression::LessEqual { .. } => ExpressionType::LessEqual,
            InternalExpression::Greater { .. } => ExpressionType::Greater,
            InternalExpression::GreaterEqual { .. } => ExpressionType::GreaterEqual,

            InternalExpression::Addition { .. } => ExpressionType::Addition,
            InternalExpression::Subtraction { .. } => ExpressionType::Subtraction,
            InternalExpression::Multiplication { .. } => ExpressionType::Multiplication,
            InternalExpression::Division { .. } => ExpressionType::Division,
            InternalExpression::Modulo { .. } => ExpressionType::Modulo,
        }
    }

    pub fn location(&self) -> &SourceLocation {
        &self.location
    }
}

impl fmt::Display for Expression {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let line_number = self.location().line();
        let column_number = self.location().column();

        write!(
            f,
            "{}",
            match &self.internal {
                InternalExpression::Identifier(value) =>
                    format!("{} {}:{}", value, line_number, column_number),

                InternalExpression::Number(value) =>
                    format!("{} {}:{}", value, line_number, column_number),
                InternalExpression::String(value) =>
                    format!(r#""{}" {}:{}"#, value, line_number, column_number),
                InternalExpression::Bool(value) =>
                    format!("{} {}:{}", value, line_number, column_number),
                InternalExpression::Nil => format!("nil {}:{}", line_number, column_number),

                InternalExpression::Index { to_index, index } => operator_format(
                    "index",
                    line_number,
                    column_number,
                    &[&to_index.to_string(), &index.to_string()]
                ),
                InternalExpression::List(list) => {
                    let list = list.iter().map(|v| v.to_string()).collect::<Vec<String>>();
                    let list = list.iter().map(AsRef::as_ref).collect::<Vec<_>>();
                    operator_format("list", line_number, column_number, list.as_slice())
                }
                InternalExpression::Range { start, end } => operator_format(
                    ":",
                    line_number,
                    column_number,
                    &[
                        &match start {
                            Some(start) => start.to_string(),
                            None => "_".to_string(),
                        },
                        &match end {
                            Some(end) => end.to_string(),
                            None => "_".to_string(),
                        }
                    ]
                ),

                InternalExpression::Call { callee, arguments } =>
                    operator_format("call", callee.location.line(), callee.location.column(), {
                        arguments
                            .iter()
                            .map(|v| v.to_string())
                            .collect::<Vec<String>>()
                            .iter()
                            .map(AsRef::as_ref)
                            .collect::<Vec<&str>>()
                            .as_slice()
                    }),
                InternalExpression::Function {
                    argument_names,
                    body,
                } => {
                    format!(
                        "fn({}) {}",
                        {
                            let mut out = String::new();
                            for name in argument_names {
                                out.push_str(name);
                                out.push_str(", ");
                            }
                            out.pop();
                            out.pop();

                            out
                        },
                        body
                    )
                }

                InternalExpression::Group(expression) =>
                    format!("(\n{}\n)", add_indentation(&format!("{}", expression))),

                InternalExpression::Assign {
                    identifier,
                    expression,
                } => operator_format(
                    "=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),
                InternalExpression::AdditionAssign {
                    identifier,
                    expression,
                } => operator_format(
                    "+=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),
                InternalExpression::SubtractionAssign {
                    identifier,
                    expression,
                } => operator_format(
                    "-=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),
                InternalExpression::MultiplicationAssign {
                    identifier,
                    expression,
                } => operator_format(
                    "*=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),
                InternalExpression::DivisionAssign {
                    identifier,
                    expression,
                } => operator_format(
                    "/=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),
                InternalExpression::ModuloAssign {
                    identifier,
                    expression,
                } => operator_format(
                    "%=",
                    line_number,
                    column_number,
                    &[&identifier, &expression.to_string()]
                ),

                InternalExpression::And { left, right } => operator_format(
                    "&&",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Or { left, right } => operator_format(
                    "||",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),

                InternalExpression::Not(expression) =>
                    operator_format("!", line_number, column_number, &[&expression.to_string()]),
                InternalExpression::Negate(expression) =>
                    operator_format("-", line_number, column_number, &[&expression.to_string()]),

                InternalExpression::Equal { left, right } => operator_format(
                    "==",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::NotEqual { left, right } => operator_format(
                    "!=",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Greater { left, right } => operator_format(
                    ">",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::GreaterEqual { left, right } => operator_format(
                    ">=",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Less { left, right } => operator_format(
                    "<",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::LessEqual { left, right } => operator_format(
                    "<=",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),

                InternalExpression::Addition { left, right } => operator_format(
                    "+",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Subtraction { left, right } => operator_format(
                    "-",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Multiplication { left, right } => operator_format(
                    "*",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Division { left, right } => operator_format(
                    "/",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
                InternalExpression::Modulo { left, right } => operator_format(
                    "%",
                    line_number,
                    column_number,
                    &[&left.to_string(), &right.to_string()]
                ),
            }
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum ExpressionType {
    Identifier,

    Number,
    String,
    Bool,
    Nil,

    Index,
    List,
    Range,

    Call,
    Function,

    Group,

    Assign,
    AdditionAssign,
    SubtractionAssign,
    MultiplicationAssign,
    DivisionAssign,
    ModuloAssign,

    And,
    Or,

    Not,
    Negate,

    Equal,
    NotEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Addition,
    Subtraction,
    Multiplication,
    Division,
    Modulo,
}

impl fmt::Display for ExpressionType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ExpressionType::Identifier => "Identifier",

                ExpressionType::Number => "Number",
                ExpressionType::String => "String",
                ExpressionType::Bool => "Bool",
                ExpressionType::Nil => "Nil",

                ExpressionType::Index => "Index",
                ExpressionType::List => "List",
                ExpressionType::Range => "Range",

                ExpressionType::Call => "Call",
                ExpressionType::Function => "Function",

                ExpressionType::Group => "Group",

                ExpressionType::Assign => "Assign",
                ExpressionType::AdditionAssign => "AdditionAssign",
                ExpressionType::SubtractionAssign => "SubtractionAssign",
                ExpressionType::MultiplicationAssign => "MultiplicationAssign",
                ExpressionType::DivisionAssign => "DivisionAssign",
                ExpressionType::ModuloAssign => "ModuloAssign",

                ExpressionType::And => "And",
                ExpressionType::Or => "Or",

                ExpressionType::Not => "Not",
                ExpressionType::Negate => "Negate",

                ExpressionType::Equal => "Equal",
                ExpressionType::NotEqual => "NotEqual",
                ExpressionType::Greater => "Greater",
                ExpressionType::GreaterEqual => "GreaterEqual",
                ExpressionType::Less => "Less",
                ExpressionType::LessEqual => "LessEqual",
                ExpressionType::Addition => "Addition",
                ExpressionType::Subtraction => "Subtraction",
                ExpressionType::Multiplication => "Multiplication",
                ExpressionType::Division => "Division",
                ExpressionType::Modulo => "Modulo",
            }
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
enum InternalExpression {
    Identifier(String),

    Number(f64),
    String(String),
    Bool(bool),
    Nil,

    Index {
        to_index: Box<Expression>,
        index: Box<Expression>,
    },
    List(Vec<Expression>),
    Range {
        start: Option<Box<Expression>>,
        end: Option<Box<Expression>>,
    },

    Call {
        callee: Box<Expression>,
        arguments: Vec<Expression>,
    },
    Function {
        argument_names: Vec<String>,
        body: Box<Statement>,
    },

    Group(Box<Expression>),

    Not(Box<Expression>),
    Negate(Box<Expression>),

    Assign {
        identifier: String,
        expression: Box<Expression>,
    },
    AdditionAssign {
        identifier: String,
        expression: Box<Expression>,
    },
    SubtractionAssign {
        identifier: String,
        expression: Box<Expression>,
    },
    MultiplicationAssign {
        identifier: String,
        expression: Box<Expression>,
    },
    DivisionAssign {
        identifier: String,
        expression: Box<Expression>,
    },
    ModuloAssign {
        identifier: String,
        expression: Box<Expression>,
    },

    And {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Or {
        left: Box<Expression>,
        right: Box<Expression>,
    },

    Equal {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    NotEqual {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Greater {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    GreaterEqual {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Less {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    LessEqual {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Addition {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Subtraction {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Multiplication {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Division {
        left: Box<Expression>,
        right: Box<Expression>,
    },
    Modulo {
        left: Box<Expression>,
        right: Box<Expression>,
    },
}

fn operator_format(
    operator: &str,
    line_number: usize,
    column_number: usize,
    expressions: &[&str],
) -> String {
    format!(
        r#"( {} {}:{}
{}
)"#,
        operator,
        line_number,
        column_number,
        {
            let mut result = String::new();

            for expression in expressions {
                result.push_str(&add_indentation(expression));
                result.push('\n');
            }
            result.pop();

            result
        }
    )
}

fn add_indentation(input: &str) -> String {
    input
        .split('\n')
        .map(|v| format!("  {}", v))
        .collect::<Vec<String>>()
        .join("\n")
}
