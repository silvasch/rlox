use crate::prelude::*;

static VALID_IDENTIFIER_CHARS: &str =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

static SINGLE_CHAR_TOKENS: phf::Map<char, TokenType> = phf::phf_map! {
    '(' => TokenType::LeftParen,
    ')' => TokenType::RightParen,
    '{' => TokenType::LeftBrace,
    '}' => TokenType::RightBrace,
    '[' => TokenType::LeftBracket,
    ']' => TokenType::RightBracket,

    ',' => TokenType::Comma,
    ':' => TokenType::Colon,
    ';' => TokenType::Semicolon,
};

static KEYWORDS: phf::Map<&'static str, TokenType> = phf::phf_map! {
    "true" => TokenType::True,
    "false" => TokenType::False,

    "if" => TokenType::If,
    "else" => TokenType::Else,
    "and" => TokenType::And,
    "or" => TokenType::Or,

    "class" => TokenType::Class,
    "this" => TokenType::This,
    "super" => TokenType::Super,

    "fn" => TokenType::Function,
    "return" => TokenType::Return,

    "for" => TokenType::For,
    "foreach" => TokenType::Foreach,
    "while" => TokenType::While,

    "nil" => TokenType::Nil,
    "var" => TokenType::Var,
};

pub struct Lexer {
    file_path: Option<String>,
    source: String,

    current_index: usize,
    current_line_number: usize,
    current_column_number: usize,
}

impl Lexer {
    pub fn new(source: &str, file_path: Option<&str>) -> Self {
        Self {
            file_path: file_path.map(|v| v.to_string()),
            source: source.trim().to_string(),

            current_index: 0,
            current_line_number: 1,
            current_column_number: 1,
        }
    }

    pub fn get_tokens(&mut self) -> Result<Vec<Token>> {
        let mut tokens = Vec::new();

        while !self.is_at_end() {
            tokens.push(self.get_token()?);
        }

        Ok(tokens)
    }

    /// Get the next token.
    /// current_index should point to the first unprocessed token, and after the function finishes it leaves the pointer at the first unprocessed token again.
    /// If get_token reaches the end of the input, it leaves current_index pointing to the first non-valid element.
    fn get_token(&mut self) -> Result<Token> {
        let current_char = self
            .peek(0)
            .expect("get_token should never get called when at the end of the iterator");

        let token = if let Some(token_type) = SINGLE_CHAR_TOKENS.get(&current_char) {
            Token::new(token_type.clone(), self.location())
        } else {
            match current_char {
                ' ' | '\t' => {
                    self.advance();
                    return self.get_token();
                }
                '\n' => {
                    self.advance();
                    self.current_line_number += 1;
                    self.current_column_number = 1;
                    return self.get_token();
                }

                '!' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::BangEqual
                    } else {
                        TokenType::Bang
                    },
                    self.location(),
                ),

                '+' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::PlusEqual
                    } else {
                        TokenType::Plus
                    },
                    self.location(),
                ),

                '-' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::MinusEqual
                    } else {
                        TokenType::Minus
                    },
                    self.location(),
                ),

                '*' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::StarEqual
                    } else {
                        TokenType::Star
                    },
                    self.location(),
                ),

                '/' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::SlashEqual
                    } else {
                        TokenType::Slash
                    },
                    self.location(),
                ),

                '%' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::PercentEqual
                    } else {
                        TokenType::Percent
                    },
                    self.location(),
                ),

                '=' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::EqualEqual
                    } else {
                        TokenType::Equal
                    },
                    self.location(),
                ),

                '>' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::GreaterEqual
                    } else {
                        TokenType::Greater
                    },
                    self.location(),
                ),

                '<' => Token::new(
                    if let Some('=') = self.peek(1) {
                        self.advance();
                        TokenType::LessEqual
                    } else {
                        TokenType::Less
                    },
                    self.location(),
                ),

                '"' => self.get_string()?,

                ch if ch.is_ascii_digit() || ch == '.' => self.get_number()?,

                _ => {
                    let identifier = self.get_identifier()?;

                    match KEYWORDS.get(
                        identifier
                            .expect_identifier()
                            .expect("get_identifier can only return identifiers"),
                    ) {
                        Some(token_type) => {
                            Token::new(token_type.clone(), identifier.location().clone())
                        }
                        None => identifier,
                    }
                }
            }
        };

        self.advance();
        Ok(token)
    }

    fn get_string(&mut self) -> Result<Token> {
        let mut result = String::new();

        let start_location = self.location();

        loop {
            match self.peek(1) {
                Some('"') => break,
                Some('\\') => {
                    self.advance();
                    let sequence = match self.peek(1).ok_or(Error::new(
                        ErrorKind::InvalidEscapeSequence(None),
                        self.location(),
                    ))? {
                        'n' => '\n',
                        't' => '\t',
                        'r' => '\r',
                        '"' => '\"',
                        '\'' => '\'',
                        '\\' => '\\',
                        '0' => '\0',
                        ch => {
                            return Err(Error::new(
                                ErrorKind::InvalidEscapeSequence(Some(ch)),
                                self.location(),
                            ))
                        }
                    };
                    self.advance();
                    result.push(sequence);
                }
                Some(ch) => {
                    self.advance();
                    result.push(ch)
                }
                None => return Err(Error::new(ErrorKind::UndelimitedString, start_location)),
            }
        }

        self.advance();

        Ok(Token::new_string(&result, start_location))
    }

    fn get_identifier(&mut self) -> Result<Token> {
        let location = self.location();

        let mut result = String::from(self.peek(0).unwrap());

        loop {
            match self.peek(1) {
                Some(ch) if VALID_IDENTIFIER_CHARS.contains(ch) => result.push(ch),
                _ => break,
            }
            self.advance();
        }

        Ok(Token::new_identifier(&result, location))
    }

    fn get_number(&mut self) -> Result<Token> {
        let location = self.location();

        let mut result = String::new();
        let mut had_dot = false;
        match self.peek(0) {
            Some('.') => match self.peek(1) {
                Some(ch) if ch.is_ascii_digit() => {
                    had_dot = true;
                    result.push_str("0.");
                }
                _ => return Ok(Token::new(TokenType::Dot, location)),
            },
            Some(ch) => result.push(ch),
            None => unreachable!(),
        }

        loop {
            match self.peek(1) {
                Some(ch) if ch.is_ascii_digit() => result.push(ch),
                Some('.') => {
                    if had_dot {
                        break;
                    } else {
                        result.push('.');
                        had_dot = true;
                    }
                }
                _ => break,
            }

            self.advance();
        }

        Ok(Token::new_number(
            result
                .parse()
                .map_err(|_| Error::new(ErrorKind::NumberParserError(result), location.clone()))?,
            location,
        ))
    }

    fn peek(&self, distance: isize) -> Option<char> {
        let index = self.current_index as isize + distance;
        if index < 0 {
            None
        } else {
            self.source.chars().nth(index as usize)
        }
    }

    fn advance(&mut self) {
        self.current_index += 1;
        self.current_column_number += 1;
    }

    fn is_at_end(&self) -> bool {
        self.peek(0).is_none()
    }

    fn location(&self) -> SourceLocation {
        let mut source_location_builder =
            SourceLocation::builder(self.current_line_number, self.current_column_number);

        if let Some(file_path) = &self.file_path {
            source_location_builder = source_location_builder.file_path(file_path);
        }

        source_location_builder = source_location_builder.snippet(
            self.source
                .lines()
                .nth(self.current_line_number - 1)
                .expect("current_line_number should always point to a valid line"),
        );

        source_location_builder.build()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn complex_test() {
        use TokenType::*;

        let source = r#"var is_prime = fn(num) {
    var i = 2;
    while true {
        i = i + 1;
        if i > num - 1 {
            return true;
        } else {
            if num % i != 0 {
                return false;
            }
        }
    }     
}"#;
        let expected = vec![
            Var, Identifier, Equal, Function, LeftParen, Identifier, RightParen, LeftBrace, Var,
            Identifier, Equal, Number, Semicolon, While, True, LeftBrace, Identifier, Equal,
            Identifier, Plus, Number, Semicolon, If, Identifier, Greater, Identifier, Minus,
            Number, LeftBrace, Return, True, Semicolon, RightBrace, Else, LeftBrace, If,
            Identifier, Percent, Identifier, BangEqual, Number, LeftBrace, Return, False,
            Semicolon, RightBrace, RightBrace, RightBrace, RightBrace,
        ];

        assert_eq!(
            expected,
            Lexer::new(source, None)
                .get_tokens()
                .unwrap_fmt()
                .iter()
                .map(|v| v.ty())
                .collect::<Vec<TokenType>>()
        )
    }

    #[test]
    fn keywords_test() {
        let source = r#"true false"#;
        let expected = vec![TokenType::True, TokenType::False];

        assert_eq!(
            expected,
            Lexer::new(source, None)
                .get_tokens()
                .unwrap_fmt()
                .iter()
                .map(|v| v.ty())
                .collect::<Vec<TokenType>>()
        )
    }

    #[test]
    fn identifier_test() {
        let source = "_foo bar";
        let expected = vec!["_foo".to_string(), "bar".to_string()];

        assert_eq!(
            expected,
            Lexer::new(source, None)
                .get_tokens()
                .unwrap_fmt()
                .iter()
                .map(|v| v.expect_identifier().unwrap_fmt().clone())
                .collect::<Vec<String>>()
        )
    }

    #[test]
    fn number_test() {
        let source = "12 34.34 .012";
        let expected = vec![12.0, 34.34, 0.012];

        assert_eq!(
            expected,
            Lexer::new(source, None)
                .get_tokens()
                .unwrap_fmt()
                .iter()
                .map(|v| v.expect_number().unwrap_fmt())
                .collect::<Vec<f64>>()
        )
    }

    #[test]
    fn operator_test() {
        let source = r#"(  )
   ][{}]
        ,.: ;
        
+- * /
<<===!
> = >=
    += %= -"#;
        let expected = vec![
            TokenType::LeftParen,
            TokenType::RightParen,
            TokenType::RightBracket,
            TokenType::LeftBracket,
            TokenType::LeftBrace,
            TokenType::RightBrace,
            TokenType::RightBracket,
            TokenType::Comma,
            TokenType::Dot,
            TokenType::Colon,
            TokenType::Semicolon,
            TokenType::Plus,
            TokenType::Minus,
            TokenType::Star,
            TokenType::Slash,
            TokenType::Less,
            TokenType::LessEqual,
            TokenType::EqualEqual,
            TokenType::Bang,
            TokenType::Greater,
            TokenType::Equal,
            TokenType::GreaterEqual,
            TokenType::PlusEqual,
            TokenType::PercentEqual,
            TokenType::Minus,
        ];

        assert_eq!(
            expected,
            Lexer::new(source, None)
                .get_tokens()
                .unwrap_fmt()
                .iter()
                .map(|v| v.ty())
                .collect::<Vec<TokenType>>()
        )
    }
}
