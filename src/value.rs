use std::fmt;

use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Value {
    internal: InternalValue,
    location: SourceLocation,
}

impl Value {
    pub fn new_number(value: f64, location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::Number(value),
            location,
        }
    }

    pub fn new_string(value: &str, location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::String(value.to_string()),
            location,
        }
    }

    pub fn new_bool(value: bool, location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::Bool(value),
            location,
        }
    }

    pub fn new_list(value: &[Value], location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::List(value.to_vec()),
            location,
        }
    }

    pub fn new_range(start: isize, end: Option<isize>, location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::Range { start, end },
            location,
        }
    }

    pub fn new_nil(location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::Nil,
            location,
        }
    }

    pub fn new_callable(value: Callable, location: SourceLocation) -> Self {
        Self {
            internal: InternalValue::Callable(value),
            location,
        }
    }

    pub fn expect_number(&self) -> Result<f64> {
        if let InternalValue::Number(value) = &self.internal {
            Ok(*value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::Number],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_string(&self) -> Result<&String> {
        if let InternalValue::String(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::String],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_list(&self) -> Result<&Vec<Value>> {
        if let InternalValue::List(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::String],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_range(&self) -> Result<(isize, Option<isize>)> {
        if let InternalValue::Range { start, end } = &self.internal {
            Ok((*start, *end))
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::String],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    pub fn expect_bool(&self) -> Result<bool> {
        if let InternalValue::Bool(value) = &self.internal {
            Ok(*value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::String],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    #[allow(dead_code)]
    pub fn expect_nil(&self) -> Result<()> {
        if let InternalValue::Nil = &self.internal {
            Ok(())
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::Nil],
                    actual: self.ty(),
                },
                self.location.clone(),
            ))
        }
    }

    #[allow(dead_code)]
    pub fn expect_callable(&self, location: &SourceLocation) -> Result<&Callable> {
        if let InternalValue::Callable(value) = &self.internal {
            Ok(value)
        } else {
            Err(Error::new(
                ErrorKind::UnexpectedValue {
                    expected: vec![ValueType::Callable],
                    actual: self.ty(),
                },
                location.clone(),
            ))
        }
    }

    pub fn ty(&self) -> ValueType {
        match self.internal {
            InternalValue::Number(_) => ValueType::Number,
            InternalValue::String(_) => ValueType::String,
            InternalValue::Bool(_) => ValueType::Bool,
            InternalValue::List(_) => ValueType::List,
            InternalValue::Range { .. } => ValueType::Range,
            InternalValue::Callable(_) => ValueType::Callable,
            InternalValue::Nil => ValueType::Nil,
        }
    }

    pub fn location(&self) -> &SourceLocation {
        &self.location
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        self.internal == other.internal
    }
}

impl From<(String, SourceLocation)> for Value {
    fn from(value: (String, SourceLocation)) -> Self {
        Self::new_string(&value.0, value.1)
    }
}

impl From<(f64, SourceLocation)> for Value {
    fn from(value: (f64, SourceLocation)) -> Self {
        Self::new_number(value.0, value.1)
    }
}

impl From<(bool, SourceLocation)> for Value {
    fn from(value: (bool, SourceLocation)) -> Self {
        Self::new_bool(value.0, value.1)
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match &self.internal {
                InternalValue::Number(value) => value.to_string(),
                InternalValue::String(value) => value.to_string(),
                InternalValue::Bool(value) => value.to_string(),
                InternalValue::List(values) => {
                    if values.is_empty() {
                        "[]".to_string()
                    } else {
                        let mut result = "[".to_string();

                        for value in values {
                            result.push_str(&value.to_string());
                            result.push_str(", ");
                        }
                        result.pop();
                        result.pop();
                        result.push(']');

                        result
                    }
                }
                InternalValue::Range { start, end } => format!(
                    "{}:{}",
                    start,
                    match end {
                        Some(end) => end.to_string(),
                        None => "".to_string(),
                    }
                ),
                InternalValue::Callable(_) => "<callable>".to_string(),
                InternalValue::Nil => "nil".to_string(),
            }
        )
    }
}

#[derive(Clone, Debug)]
pub enum ValueType {
    Number,
    String,
    Bool,
    List,
    Range,
    Callable,
    Nil,
}

impl fmt::Display for ValueType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ValueType::Number => "Number",
                ValueType::String => "String",
                ValueType::Bool => "Bool",
                ValueType::List => "List",
                ValueType::Range => "Range",
                ValueType::Callable => "Callable",
                ValueType::Nil => "Nil",
            }
        )
    }
}

#[derive(Clone, Debug, PartialEq)]
enum InternalValue {
    Number(f64),
    String(String),
    Bool(bool),
    List(Vec<Value>),
    Range { start: isize, end: Option<isize> },
    Callable(Callable),
    Nil,
}

pub trait ExpectArity {
    fn expect_arity<const N: usize>(&self, location: &SourceLocation) -> Result<&[Value; N]>;

    fn expect_min_arity(&self, min: usize, location: &SourceLocation) -> Result<&[Value]>;

    fn expect_max_arity(&self, max: usize, location: &SourceLocation) -> Result<&[Value]>;
}

impl ExpectArity for &[Value] {
    fn expect_arity<const N: usize>(&self, location: &SourceLocation) -> Result<&[Value; N]> {
        if self.len() == N {
            Ok(std::convert::TryInto::<&[Value; N]>::try_into(*self).unwrap())
        } else {
            Err(Error::new(
                ErrorKind::ExpectExactArity {
                    expected: N,
                    actual: self.len(),
                },
                location.clone(),
            ))
        }
    }

    fn expect_min_arity(&self, min: usize, location: &SourceLocation) -> Result<&[Value]> {
        if self.len() >= min {
            Ok(self)
        } else {
            Err(Error::new(
                ErrorKind::ExpectMinArity {
                    min,
                    actual: self.len(),
                },
                location.clone(),
            ))
        }
    }

    fn expect_max_arity(&self, max: usize, location: &SourceLocation) -> Result<&[Value]> {
        if self.len() <= max {
            Ok(self)
        } else {
            Err(Error::new(
                ErrorKind::ExpectMaxArity {
                    max,
                    actual: self.len(),
                },
                location.clone(),
            ))
        }
    }
}
