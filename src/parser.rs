use crate::prelude::*;

pub struct Parser {
    tokens: Vec<Token>,

    current_index: usize,

    last_location: SourceLocation,
}

impl Parser {
    pub fn new(tokens: Vec<Token>, last_location: SourceLocation) -> Self {
        Self {
            tokens,
            current_index: 0,
            last_location,
        }
    }

    pub fn get_statements(&mut self) -> Result<Vec<Statement>> {
        let mut statements = Vec::new();

        while self.peek(0).is_some() {
            statements.push(self.get_declaration()?);
        }

        Ok(statements)
    }

    fn get_declaration(&mut self) -> Result<Statement> {
        let current_token = self.peek(0).unwrap();

        if current_token.expect(&[TokenType::Var]).is_ok() {
            self.get_var_declaration()
        } else {
            self.get_statement()
        }
    }

    fn get_var_declaration(&mut self) -> Result<Statement> {
        let location = self.peek(0).unwrap().location().clone();

        self.advance();

        let identifier = self
            .peek(0)
            .expect_token(&[TokenType::Identifier], &self.last_location)?
            .expect_identifier()?
            .clone();
        self.advance();

        self.expect_and_consume(&[TokenType::Equal])?;

        let expression = self
            .get_expression()?
            .expect_expression(&self.last_location)?;

        self.expect_and_consume(&[TokenType::Semicolon])?;

        Ok(Statement::new_var(&identifier, expression, location))
    }

    fn get_return(&mut self) -> Result<Statement> {
        let location = self.peek(0).unwrap().location().clone();
        self.advance();
        let expression = self
            .get_expression()?
            .expect_expression(&self.last_location)?;
        self.expect_and_consume(&[TokenType::Semicolon])?;
        Ok(Statement::new_return(expression, location))
    }

    fn get_statement(&mut self) -> Result<Statement> {
        let token = self
            .peek(0)
            .cloned()
            .expect("should not get called when at the end of the iterator");
        let location = token.location();

        let statement = match self
            .peek(0)
            .expect("should not get called when at the end of the iterator")
            .ty()
        {
            TokenType::If => {
                self.advance();
                self.expect_and_consume(&[TokenType::LeftParen])?;

                let condition = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;

                self.expect_and_consume(&[TokenType::RightParen])?;

                let then_statement = self.get_statement()?;

                let mut else_statement = None;

                if self.peek(0).is_some()
                    && self.peek(0).unwrap().expect(&[TokenType::Else]).is_ok()
                {
                    self.advance();
                    else_statement = Some(Box::new(self.get_statement()?));
                }

                Statement::new_if(
                    condition,
                    Box::new(then_statement),
                    else_statement,
                    location.clone(),
                )
            }
            TokenType::Return => self.get_return()?,
            TokenType::While => {
                self.advance();
                self.expect_and_consume(&[TokenType::LeftParen])?;

                let condition = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;

                self.expect_and_consume(&[TokenType::RightParen])?;

                let statement = self.get_statement()?;

                Statement::new_while(condition, Box::new(statement), location.clone())
            }
            TokenType::For => {
                self.advance();
                self.expect_and_consume(&[TokenType::LeftParen])?;

                let decl = self.get_var_declaration()?;
                decl.expect_var()?;

                let condition = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;
                self.expect_and_consume(&[TokenType::Semicolon])?;

                let increment = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;
                if increment.expect_assign().is_err()
                    && increment.expect_addition_assign().is_err()
                    && increment.expect_subtraction_assign().is_err()
                    && increment.expect_multiplication_assign().is_err()
                    && increment.expect_division_assign().is_err()
                    && increment.expect_modulo_assign().is_err()
                {
                    return Err(Error::new(
                        ErrorKind::UnexpectedExpression {
                            expected: vec![
                                ExpressionType::Assign,
                                ExpressionType::AdditionAssign,
                                ExpressionType::SubtractionAssign,
                                ExpressionType::MultiplicationAssign,
                                ExpressionType::SubtractionAssign,
                                ExpressionType::ModuloAssign,
                            ],
                            actual: increment.ty(),
                        },
                        increment.location().clone(),
                    ));
                }
                self.expect_and_consume(&[TokenType::RightParen])?;

                let statement = self.get_statement()?;

                Statement::new_for(
                    Box::new(decl),
                    condition,
                    increment,
                    Box::new(statement),
                    location.clone(),
                )
            }
            TokenType::Foreach => {
                self.advance();
                self.expect_and_consume(&[TokenType::LeftParen])?;

                let value = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;

                self.expect_and_consume(&[TokenType::Semicolon])?;

                let capture = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?
                    .expect_identifier()?
                    .clone();

                self.expect_and_consume(&[TokenType::RightParen])?;

                let statement = self.get_statement()?;

                Statement::new_foreach(value, &capture, Box::new(statement), location.clone())
            }

            TokenType::LeftBrace => Statement::new_block(self.get_block()?, location.clone()),
            _ => {
                let expression = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;
                self.expect_and_consume(&[TokenType::Semicolon])?;
                Statement::new_expression(expression, location.clone())
            }
        };

        Ok(statement)
    }

    fn get_block(&mut self) -> Result<Vec<Statement>> {
        let mut statements = vec![];

        self.advance();

        while self
            .peek(0)
            .expect_token(&[], &self.last_location)?
            .expect(&[TokenType::RightBrace])
            .is_err()
        {
            statements.push(self.get_declaration()?);
        }

        self.advance();

        Ok(statements)
    }

    fn get_expression(&mut self) -> Result<Option<Expression>> {
        self.get_range()
    }

    fn get_range(&mut self) -> Result<Option<Expression>> {
        let mut left = self.get_function()?.map(Box::new);

        while let Some(operator) = self.peek(0).cloned() {
            if operator.expect(&[TokenType::Colon]).is_err() {
                break;
            }

            self.advance();

            let right = self.get_function()?.map(Box::new);

            let location = operator.location().clone();
            left = Some(Box::new(Expression::new_range(left, right, location)));
        }

        let left = left.map(|left| *left);

        Ok(left)
    }

    fn get_function(&mut self) -> Result<Option<Expression>> {
        if let Ok(fn_token) = self.expect_and_consume(&[TokenType::Function]).cloned() {
            self.expect_and_consume(&[TokenType::LeftParen])?;

            let mut arguments = vec![];

            loop {
                let argument = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?
                    .expect_identifier()?
                    .clone();
                arguments.push(argument);

                match self.expect_and_consume(&[TokenType::Comma]) {
                    Ok(_) => {}
                    Err(_) => break,
                }
            }

            self.expect_and_consume(&[TokenType::RightParen])?;

            let body = self.get_statement()?;

            Ok(Some(Expression::new_function(
                arguments
                    .iter()
                    .map(|v| v.as_str())
                    .collect::<Vec<&str>>()
                    .as_slice(),
                body,
                fn_token.location().clone(),
            )))
        } else {
            self.get_assignement()
        }
    }

    fn get_assignement(&mut self) -> Result<Option<Expression>> {
        let expression = match self.get_or()? {
            Some(expression) => expression,
            None => return Ok(None),
        };

        if let Some(token) = self.peek(0).cloned() {
            if token
                .expect(&[
                    TokenType::Equal,
                    TokenType::PlusEqual,
                    TokenType::MinusEqual,
                    TokenType::StarEqual,
                    TokenType::SlashEqual,
                    TokenType::Percent,
                ])
                .is_ok()
            {
                self.advance();
                let value = match self.get_assignement()? {
                    Some(value) => value,
                    None => return Ok(None),
                };

                let identifier = expression.expect_identifier()?;

                let constructor = if token.expect(&[TokenType::Equal]).is_ok() {
                    Expression::new_assign
                } else if token.expect(&[TokenType::PlusEqual]).is_ok() {
                    Expression::new_addition_assign
                } else if token.expect(&[TokenType::MinusEqual]).is_ok() {
                    Expression::new_subtraction_assign
                } else if token.expect(&[TokenType::StarEqual]).is_ok() {
                    Expression::new_multiplication_assign
                } else if token.expect(&[TokenType::SlashEqual]).is_ok() {
                    Expression::new_division_assign
                } else if token.expect(&[TokenType::PercentEqual]).is_ok() {
                    Expression::new_modulo_assign
                } else {
                    unreachable!()
                };

                return Ok(Some(constructor(
                    identifier.to_string(),
                    value,
                    expression.location().clone(),
                )));
            }
        }

        Ok(Some(expression))
    }

    fn get_or(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_and()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator.expect(&[TokenType::Or]).is_err() {
                break;
            }

            self.advance();

            let right_expression = match self.get_or()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                ExpressionType::Or,
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_and(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_equality()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator.expect(&[TokenType::And]).is_err() {
                break;
            }

            self.advance();

            let right_expression = match self.get_and()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                ExpressionType::And,
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_equality(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_comparison()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator
                .expect(&[TokenType::BangEqual, TokenType::EqualEqual])
                .is_err()
            {
                break;
            }

            self.advance();

            let right_expression = match self.get_comparison()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                match operator.ty() {
                    TokenType::BangEqual => ExpressionType::NotEqual,
                    TokenType::EqualEqual => ExpressionType::Equal,
                    _ => unreachable!(),
                },
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_comparison(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_term()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator
                .expect(&[
                    TokenType::Greater,
                    TokenType::GreaterEqual,
                    TokenType::Less,
                    TokenType::LessEqual,
                ])
                .is_err()
            {
                break;
            }

            self.advance();

            let right_expression = match self.get_term()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                match operator.ty() {
                    TokenType::Greater => ExpressionType::Greater,
                    TokenType::GreaterEqual => ExpressionType::GreaterEqual,
                    TokenType::Less => ExpressionType::Less,
                    TokenType::LessEqual => ExpressionType::LessEqual,
                    _ => unreachable!(),
                },
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_term(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_factor()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator
                .expect(&[TokenType::Plus, TokenType::Minus])
                .is_err()
            {
                break;
            }

            self.advance();

            let right_expression = match self.get_factor()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                match operator.ty() {
                    TokenType::Plus => ExpressionType::Addition,
                    TokenType::Minus => ExpressionType::Subtraction,
                    _ => unreachable!(),
                },
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_factor(&mut self) -> Result<Option<Expression>> {
        let mut left_expression = match self.get_unary()? {
            Some(left_expression) => left_expression,
            None => return Ok(None),
        };

        while let Some(operator) = self.peek(0).cloned() {
            if operator
                .expect(&[TokenType::Star, TokenType::Slash, TokenType::Percent])
                .is_err()
            {
                break;
            }

            self.advance();

            let right_expression = match self.get_unary()? {
                Some(right_expression) => right_expression,
                None => return Ok(None),
            };

            let location = operator.location().clone();
            left_expression = Expression::new_binary(
                match operator.ty() {
                    TokenType::Star => ExpressionType::Multiplication,
                    TokenType::Slash => ExpressionType::Division,
                    TokenType::Percent => ExpressionType::Modulo,
                    _ => unreachable!(),
                },
                left_expression,
                right_expression,
                location,
            );
        }

        Ok(Some(left_expression))
    }

    fn get_unary(&mut self) -> Result<Option<Expression>> {
        if let Some(operator) = self.peek(0).cloned() {
            if operator
                .expect(&[TokenType::Bang, TokenType::Minus])
                .is_ok()
            {
                self.advance();
                let expression = match self.get_unary()? {
                    Some(expression) => expression,
                    None => return Ok(None),
                };
                let location = operator.location().clone();
                return Ok(Some(Expression::new_unary(
                    match operator.ty() {
                        TokenType::Bang => ExpressionType::Not,
                        TokenType::Minus => ExpressionType::Negate,
                        _ => unreachable!(),
                    },
                    expression,
                    location,
                )));
            }
        }

        self.get_call()
    }

    fn get_call(&mut self) -> Result<Option<Expression>> {
        let callee = match self.get_index()? {
            Some(callee) => callee,
            None => return Ok(None),
        };

        match self.expect_and_consume(&[TokenType::LeftParen]).cloned() {
            Ok(left_paren) => {
                let mut arguments = Vec::new();
                loop {
                    if self.expect_and_consume(&[TokenType::RightParen]).is_ok() {
                        break;
                    }

                    let expression = self
                        .get_expression()?
                        .expect_expression(&self.last_location)?;
                    arguments.push(expression);

                    if let Ok(token) = self.peek(0).expect_token(
                        &[TokenType::Comma, TokenType::RightParen],
                        &self.last_location,
                    ) {
                        if token.expect(&[TokenType::RightParen]).is_ok() {
                            self.advance();
                            break;
                        }
                        if token.expect(&[TokenType::Comma]).is_ok() {
                            self.advance();
                            continue;
                        }
                        return Err(Error::new(
                            ErrorKind::UnexpectedToken {
                                expected: vec![TokenType::Comma, TokenType::RightParen],
                                actual: token.ty().clone(),
                            },
                            token.location().clone(),
                        ));
                    }
                }

                Ok(Some(Expression::new_call(
                    callee,
                    arguments.as_slice(),
                    left_paren.location().clone(),
                )))
            }
            Err(_) => Ok(Some(callee)),
        }
    }

    fn get_index(&mut self) -> Result<Option<Expression>> {
        let to_index = match self.get_primary()? {
            Some(to_index) => to_index,
            None => return Ok(None),
        };

        match self.expect_and_consume(&[TokenType::LeftBracket]).cloned() {
            Ok(left_paren) => {
                let index = self
                    .get_expression()?
                    .expect_expression(&self.last_location)?;
                self.expect_and_consume(&[TokenType::RightBracket])?;
                Ok(Some(Expression::new_index(
                    to_index,
                    index,
                    left_paren.location().clone(),
                )))
            }
            Err(_) => Ok(Some(to_index)),
        }
    }

    fn get_primary(&mut self) -> Result<Option<Expression>> {
        let token = self.peek(0).expect_token(&[], &self.last_location)?.clone();

        if token.expect(&[TokenType::False]).is_ok() {
            self.advance();
            return Ok(Some(Expression::new_bool(false, token.location().clone())));
        };
        if token.expect(&[TokenType::True]).is_ok() {
            self.advance();
            return Ok(Some(Expression::new_bool(true, token.location().clone())));
        };
        if token.expect(&[TokenType::Nil]).is_ok() {
            self.advance();
            return Ok(Some(Expression::new_nil(token.location().clone())));
        };

        if let Ok(value) = token.expect_number() {
            self.advance();
            return Ok(Some(Expression::new_number(
                value,
                token.location().clone(),
            )));
        }
        if let Ok(value) = token.expect_string() {
            self.advance();
            return Ok(Some(Expression::new_string(
                value,
                token.location().clone(),
            )));
        }

        if token.expect(&[TokenType::LeftParen]).is_ok() {
            self.advance();
            if self
                .peek(0)
                .expect_token(&[], &self.last_location)?
                .expect(&[TokenType::RightParen])
                .is_ok()
            {
                return Ok(Some(Expression::new_group(
                    Expression::new_nil(token.location().clone()),
                    token.location().clone(),
                )));
            }

            let expression = self
                .get_expression()?
                .expect_expression(&self.last_location)?;
            self.peek(0)
                .expect_token(&[TokenType::RightParen], &self.last_location)?
                .expect(&[TokenType::RightParen])?;
            self.advance();
            return Ok(Some(Expression::new_group(
                expression,
                token.location().clone(),
            )));
        }

        if let Ok(value) = token.expect_identifier() {
            self.advance();
            return Ok(Some(Expression::new_identifier(
                value,
                token.location().clone(),
            )));
        }

        if token.expect(&[TokenType::LeftBracket]).is_ok() {
            self.advance();

            let mut list = vec![];

            loop {
                if let Some(token) = self.peek(0) {
                    if token.expect(&[TokenType::RightBracket]).is_ok() {
                        self.advance();
                        break;
                    }
                }

                let argument = self
                    .get_expression()?
                    .clone()
                    .expect_expression(&self.last_location)?;
                list.push(argument);

                if self
                    .peek(0)
                    .expect_token(
                        &[TokenType::Comma, TokenType::RightBracket],
                        &self.last_location,
                    )?
                    .expect(&[TokenType::Comma])
                    .is_ok()
                {
                    self.advance();
                    continue;
                }
            }

            return Ok(Some(Expression::new_list(
                list.as_slice(),
                token.location().clone(),
            )));
        }

        Ok(None)
    }

    fn advance(&mut self) {
        self.current_index += 1;
    }

    fn expect_and_consume(&mut self, tys: &[TokenType]) -> Result<&Token> {
        self.peek(0)
            .expect_token(tys, &self.last_location)?
            .expect(tys)?;
        self.advance();

        Ok(self.peek(-1).unwrap())
    }

    fn peek(&self, distance: isize) -> Option<&Token> {
        let index = self.current_index as isize + distance;
        if index < 0 {
            None
        } else {
            self.tokens.get(index as usize)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn function_test() {
        let source = "var foo = fn(a) {};";
        let expected = Statement::new_var(
            "foo",
            Expression::new_function(
                &["a"],
                Statement::new_block(vec![], (1, 17).into()),
                (1, 11).into(),
            ),
            (1, 1).into(),
        );

        let last_line_location = crate::utils::last_location_from_source(source, None);

        assert_eq!(
            expected,
            Parser::new(
                crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
                last_line_location,
            )
            .get_statements()
            .unwrap_fmt()[0]
        )
    }

    #[test]
    fn lists_test() {
        let source = r#"[2, "foo", true][:1]"#;
        let expected = Expression::new_index(
            Expression::new_list(
                &[
                    Expression::new_number(2.0, (1, 2).into()),
                    Expression::new_string("foo", (1, 5).into()),
                    Expression::new_bool(true, (1, 12).into()),
                ],
                (1, 1).into(),
            ),
            Expression::new_range(
                None,
                Some(Box::new(Expression::new_number(1.0, (1, 19).into()))),
                (1, 18).into(),
            ),
            (1, 17).into(),
        );
        let last_line_location = crate::utils::last_location_from_source(source, None);

        let actual = Parser::new(
            crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
            last_line_location,
        )
        .get_expression()
        .unwrap_fmt()
        .unwrap();

        println!("{:#?}\n\n{:#?}", expected, actual);

        assert_eq!(expected, actual)
    }

    #[test]
    fn for_test() {
        let source = r#"for (var i = 0; i < 10; i = i + 1) print(i);"#;
        let expected = Statement::new_for(
            Box::new(Statement::new_var(
                "i",
                Expression::new_number(0.0, (1, 14).into()),
                (1, 6).into(),
            )),
            Expression::new_binary(
                ExpressionType::Less,
                Expression::new_identifier("i", (1, 17).into()),
                Expression::new_number(10.0, (1, 21).into()),
                (1, 19).into(),
            ),
            Expression::new_assign(
                "i".to_string(),
                Expression::new_binary(
                    ExpressionType::Addition,
                    Expression::new_identifier("i", (1, 29).into()),
                    Expression::new_number(1.0, (1, 33).into()),
                    (1, 31).into(),
                ),
                (1, 25).into(),
            ),
            Box::new(Statement::new_expression(
                Expression::new_call(
                    Expression::new_identifier("print", (1, 36).into()),
                    &[Expression::new_identifier("i", (1, 42).into())],
                    (1, 41).into(),
                ),
                (1, 36).into(),
            )),
            (1, 1).into(),
        );

        let last_line_location = crate::utils::last_location_from_source(source, None);

        let actual = &Parser::new(
            crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
            last_line_location,
        )
        .get_statements()
        .unwrap_fmt()[0];

        println!("{:#?}\n\n{:#?}", expected, actual);

        assert_eq!(&expected, actual)
    }

    #[test]
    fn block_test() {
        let source = "{ var x = 2; x; }";
        let expected = Statement::new_block(
            vec![
                Statement::new_var(
                    "x",
                    Expression::new_number(2.0, (1, 11).into()),
                    (1, 3).into(),
                ),
                Statement::new_expression(
                    Expression::new_identifier("x", (1, 14).into()),
                    (1, 14).into(),
                ),
            ],
            (1, 1).into(),
        );

        let last_line_location = crate::utils::last_location_from_source(source, None);

        assert_eq!(
            expected,
            Parser::new(
                crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
                last_line_location,
            )
            .get_statements()
            .unwrap_fmt()[0]
        )
    }

    #[test]
    fn return_test() {
        let source = "return 42;";
        let expected =
            Statement::new_return(Expression::new_number(42.0, (1, 8).into()), (1, 1).into());

        let last_line_location = crate::utils::last_location_from_source(source, None);

        assert_eq!(
            expected,
            Parser::new(
                crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
                last_line_location,
            )
            .get_statements()
            .unwrap_fmt()[0]
        )
    }

    #[test]
    fn empty_group_test() {
        let source = "()";
        let expected = Expression::new_group(Expression::new_nil((1, 1).into()), (1, 1).into());

        let last_line_location = crate::utils::last_location_from_source(source, None);

        assert_eq!(
            expected,
            Parser::new(
                crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
                last_line_location,
            )
            .get_expression()
            .unwrap_fmt()
            .unwrap()
        )
    }

    #[test]
    fn assign_test() {
        let source = "x = 2;\nx += 1;";
        let expected = vec![
            Statement::new_expression(
                Expression::new_assign(
                    "x".to_string(),
                    Expression::new_number(2.0, (1, 5).into()),
                    (1, 1).into(),
                ),
                (1, 1).into(),
            ),
            Statement::new_expression(
                Expression::new_addition_assign(
                    "x".to_string(),
                    Expression::new_number(1.0, (2, 6).into()),
                    (2, 1).into(),
                ),
                (2, 1).into(),
            ),
        ];

        let last_line_location = crate::utils::last_location_from_source(source, None);

        let actual = Parser::new(
            crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
            last_line_location,
        )
        .get_statements()
        .unwrap_fmt();

        println!("{:#?}\n{:#?}", expected, actual);

        assert_eq!(expected, actual,)
    }

    #[test]
    fn binary_test() {
        let source = "(2 + 3) * 4";
        let expected = Expression::new_binary(
            ExpressionType::Multiplication,
            Expression::new_group(
                Expression::new_binary(
                    ExpressionType::Addition,
                    Expression::new_number(2.0, (1, 2).into()),
                    Expression::new_number(3.0, (1, 6).into()),
                    (1, 4).into(),
                ),
                (1, 1).into(),
            ),
            Expression::new_number(4.0, (1, 11).into()),
            (1, 9).into(),
        );

        let last_line_location = crate::utils::last_location_from_source(source, None);

        assert_eq!(
            expected,
            Parser::new(
                crate::Lexer::new(source, None).get_tokens().unwrap_fmt(),
                last_line_location,
            )
            .get_expression()
            .unwrap_fmt()
            .unwrap()
        )
    }
}
