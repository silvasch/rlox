use std::fmt;

use crate::prelude::*;

#[derive(Clone, Debug)]
pub struct Error {
    kind: ErrorKind,
    location: SourceLocation,
}

impl Error {
    pub fn new(kind: ErrorKind, location: SourceLocation) -> Self {
        Self { kind, location }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result = format!("error at {}: {}", self.location, self.kind);

        match self.location.snippet() {
            Some(snippet) => {
                let mut spacer = String::new();
                for _ in 0..self.location.column() + 2 {
                    spacer.push(' ');
                }

                result.push_str(&format!("\n-> {}\n{}^here", snippet, spacer))
            }
            None => {}
        }

        write!(f, "{}", result)
    }
}

#[derive(Clone, Debug)]
pub enum ErrorKind {
    EmptyRange,
    RangeStartGreaterThanEnd,

    UndefinedVariable(String),
    ReturnOutsideFunction,

    InvalidIndex {
        index: isize,
        length: usize,
    },

    UnexpectedChar {
        expected: Vec<char>,
        actual: char,
    },
    UnexpectedToken {
        expected: Vec<TokenType>,
        actual: TokenType,
    },
    UnexpectedExpression {
        expected: Vec<ExpressionType>,
        actual: ExpressionType,
    },
    ExpectedExpression,
    UnexpectedStatement {
        expected: Vec<StatementType>,
        actual: StatementType,
    },
    UnexpectedValue {
        expected: Vec<ValueType>,
        actual: ValueType,
    },

    ExpectExactArity {
        expected: usize,
        actual: usize,
    },

    ExpectMinArity {
        min: usize,
        actual: usize,
    },

    ExpectMaxArity {
        max: usize,
        actual: usize,
    },

    NumberParserError(String),
    UndelimitedString,
    InvalidEscapeSequence(Option<char>),

    Custom(String),
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ErrorKind::EmptyRange =>
                    "expected at least a start or an end expression in the range".to_string(),
                ErrorKind::RangeStartGreaterThanEnd =>
                    "the start of the range is bigger than the end".to_string(),
                ErrorKind::UndefinedVariable(identifier) =>
                    format!("tried to access an undefined variable: {}", identifier),
                ErrorKind::ReturnOutsideFunction =>
                    "encountered return statement outside of a function".to_string(),
                ErrorKind::InvalidIndex { index, length } => format!(
                    "tried to index a list of length {} with an index of {}",
                    length, index
                ),

                ErrorKind::UnexpectedChar { expected, actual } => {
                    let mut result = "unexpected char: ".to_string();

                    result.push_str(&match expected.len() {
                        0 => format!("found '{}'", actual),
                        1 => format!("expected '{}', found '{}'", expected[0], actual),
                        _ => format!(
                            "expected one of {}, found '{}'",
                            {
                                let mut result = "(".to_string();

                                for expected in expected {
                                    result.push_str(&format!("'{}', ", expected));
                                }
                                result.pop();
                                result.pop();
                                result.push(')');

                                result
                            },
                            actual
                        ),
                    });

                    result
                }
                ErrorKind::UnexpectedToken { expected, actual } => {
                    let mut result = "unexpected token: ".to_string();

                    result.push_str(&match expected.len() {
                        0 => format!("found {}", actual),
                        1 => format!("expected {}, found {}", expected[0], actual),
                        _ => format!(
                            "expected one of {}, found {}",
                            {
                                let mut result = "(".to_string();

                                for expected in expected {
                                    result.push_str(&format!("{}, ", expected));
                                }
                                result.pop();
                                result.pop();
                                result.push(')');

                                result
                            },
                            actual
                        ),
                    });

                    result
                }
                ErrorKind::UnexpectedExpression { expected, actual } => {
                    let mut result = "unexpected expression: ".to_string();

                    result.push_str(&match expected.len() {
                        0 => format!("found {}", actual),
                        1 => format!("expected {}, found {}", expected[0], actual),
                        _ => format!(
                            "expected one of {}, found {}",
                            {
                                let mut result = "(".to_string();

                                for expected in expected {
                                    result.push_str(&format!("{}, ", expected));
                                }
                                result.pop();
                                result.pop();
                                result.push(')');

                                result
                            },
                            actual
                        ),
                    });

                    result
                }
                ErrorKind::ExpectedExpression => "expected an expression".to_string(),
                ErrorKind::UnexpectedStatement { expected, actual } => {
                    let mut result = "unexpected statement: ".to_string();

                    result.push_str(&match expected.len() {
                        0 => format!("found {}", actual),
                        1 => format!("expected {}, found {}", expected[0], actual),
                        _ => format!(
                            "expected one of {}, found {}",
                            {
                                let mut result = "(".to_string();

                                for expected in expected {
                                    result.push_str(&format!("{}, ", expected));
                                }
                                result.pop();
                                result.pop();
                                result.push(')');

                                result
                            },
                            actual
                        ),
                    });

                    result
                }
                ErrorKind::UnexpectedValue { expected, actual } => {
                    let mut result = "unexpected value: ".to_string();

                    result.push_str(&match expected.len() {
                        0 => format!("found {}", actual),
                        1 => format!("expected {}, found {}", expected[0], actual),
                        _ => format!(
                            "expected one of {}, found {}",
                            {
                                let mut result = "(".to_string();

                                for expected in expected {
                                    result.push_str(&format!("{}, ", expected));
                                }
                                result.pop();
                                result.pop();
                                result.push(')');

                                result
                            },
                            actual
                        ),
                    });

                    result
                }

                ErrorKind::ExpectExactArity { expected, actual } =>
                    format!("expected {} argument(s), found {}", expected, actual),
                ErrorKind::ExpectMinArity { min, actual } =>
                    format!("expected at least {} argument(s), found {}", min, actual),
                ErrorKind::ExpectMaxArity { max, actual } => format!(
                    "expected a maximum of {} argument(s), found {}",
                    max, actual
                ),

                ErrorKind::NumberParserError(value) =>
                    format!("failed to parse '{}' as a number", value),
                ErrorKind::UndelimitedString => "undelimited string".to_string(),
                ErrorKind::InvalidEscapeSequence(sequence) => match sequence {
                    Some(ch) => format!("invalid escape sequence: \\{}", ch),
                    None => "unfinished escape sequence".to_string(),
                },
                ErrorKind::Custom(string) => string.to_string(),
            }
        )
    }
}
