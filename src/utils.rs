use crate::prelude::*;

pub fn last_location_from_source(source: &str, file_path: Option<&str>) -> SourceLocation {
    let lines = source.lines().collect::<Vec<&str>>();
    let line_count = lines.len();
    let last_line = lines.last().unwrap();
    let last_line_column_count = last_line.len() + 1;

    let mut builder =
        SourceLocation::builder(line_count, last_line_column_count).snippet(last_line);

    if let Some(file_path) = file_path {
        builder = builder.file_path(file_path);
    }

    builder.build()
}
