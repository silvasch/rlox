fn main() {
    let source = include_str!("script.lox");
    if let Err(e) = rlox::Runner::new(Some("script.lox"), false, false, true).run(source) {
        eprintln!("{}", e);
    }
}
