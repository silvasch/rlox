# rlox

This is my implementation of the language described in [Crafting Interpreters](craftinginterpreters.com).

## Installation

1. Install rust using rustup. See instructions on [rustup.rs](rustup.rs).

2. Download and install this project:
```bash
cargo install --git https://gitlab.com/silvasch/rlox --tag v0.1.0
```
